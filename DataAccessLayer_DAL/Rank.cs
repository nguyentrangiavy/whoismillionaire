﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer_DAL
{
    public class Rank
    {
        public int id { get; set; }
        public decimal reward { get; set; }
        public DateTime date { get; set; }
        public int userId { get; set; }
    }
}
