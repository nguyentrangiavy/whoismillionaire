﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer_DAL
{
    public class Question
    {
        public int id { get; set; }
        public String content { get; set; }
        public String answerA { get; set; }
        public String answerB { get; set; }
        public String answerC { get; set; }
        public String answerD { get; set; }
        public String result { get; set; }
        public String level { get; set; }
    }
}
