﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer_DAL
{
    public class UserRank
    {
        public long Rank { get; set; }
        public string Name { get; set; }
        public decimal Reward { get; set; }
        public DateTime Time { get; set; }
    }
}
