﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer_DAL
{
    public class QuestionDAO:GeneralDAO
    {
        public DataSet getAllQuestion()
        {
            return getAll("QUESTION");
        }
        public DataSet getEasyQuestions()
        {
            return getAll("vw_EasyQuestion");
        }
        public DataSet getMediumQuestions()
        {
            return getAll("vw_MediumQuestion");
        }
        public DataSet getHardQuestions()
        {
            return getAll("vw_HardQuestion");
        }
        public bool checkExist(String condition)
        {
            SqlDataReader dr =  findBy("QUESTION",condition);
            if (dr.Read())
            {
                return true;
            }
            return false;
        }
        public int insertQuestion(Question question)
        {
            SqlCommand cmd = new SqlCommand("PRC_ADD_QUESTION", GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@questioncontent", question.content);
            cmd.Parameters.AddWithValue("@answerA", question.answerA);
            cmd.Parameters.AddWithValue("@answerB", question.answerB);
            cmd.Parameters.AddWithValue("@answerC", question.answerC);
            cmd.Parameters.AddWithValue("@answerD", question.answerD);
            cmd.Parameters.AddWithValue("@correctanswer", question.result);
            cmd.Parameters.AddWithValue("@level", question.level);
            int result = cmd.ExecuteNonQuery();
            return result;
        }

        public int updateQuestion(Question question)
        {
            SqlCommand cmd = new SqlCommand("PRC_UPDATE_QUESTION", GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", question.id);
            cmd.Parameters.AddWithValue("@questioncontent", question.content);
            cmd.Parameters.AddWithValue("@answerA", question.answerA);
            cmd.Parameters.AddWithValue("@answerB", question.answerB);
            cmd.Parameters.AddWithValue("@answerC", question.answerC);
            cmd.Parameters.AddWithValue("@answerD", question.answerD);
            cmd.Parameters.AddWithValue("@correctanswer", question.result);
            cmd.Parameters.AddWithValue("@level", question.level);
            int result = cmd.ExecuteNonQuery();
            return result;
        }

        public int deleteQuestion(int id)
        {
            SqlCommand cmd = new SqlCommand("PRC_DELETE_QUESTION", GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", id);
            int result = cmd.ExecuteNonQuery();
            return result;
        }
        
    }
}
