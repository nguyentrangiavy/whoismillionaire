﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer_DAL
{
    public class RankDAO:GeneralDAO
    {
        public int createRank(Rank rank)
        {
            return insert_update_delete("INSERT INTO RANK VALUES(" + rank.reward + ",'" + rank.date.ToString("s", DateTimeFormatInfo.InvariantInfo) + "'," + rank.userId + ");");
        }
        public int updateRank(Rank rank)
        {
            return insert_update_delete("UPDATE RANK SET Reward=" + rank.reward + ", Time='" + rank.date.ToString("s", DateTimeFormatInfo.InvariantInfo) + "' WHERE User_ID=" + rank.userId + ";");
        }

        public DataSet getRanks()
        {
            return getAll("vw_UserRank");
        }

        public bool checkExist(int id)
        {
            SqlDataReader dr = findBy("RANK", "User_ID = " + id);
            if (dr.Read())
            {
                return true;
            }
            return false;
        }
        public bool checkReward(decimal reward, int id)
        {
            SqlDataReader dr = findBy("RANK", "Reward < " + reward + "AND User_ID = " + id);
            if (dr.Read())
            {
                return true;
            }
            return false;
        }
        
    }
}
