﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer_DAL
{
    public class GeneralDAO
    {
        public string connectionString = "Data Source=.;user id=sa;" + "password=1;initial catalog=WhoIsMillionaire";
        SqlConnection con = null;
        public SqlConnection GetConnection()
        {
            if (con == null)
            {
                con = new SqlConnection(connectionString);
                con.Open();
            }
            else
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
            }

            return con;
        }
        public void closeConnection()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
        public DataSet getAll(string tbtName)
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM " + tbtName, connectionString);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;

        }
        public SqlDataReader findBy(string tblName, string where_primarykey_id)
        {
            
            string sql = "SELECT * FROM " + tblName + " WHERE " + where_primarykey_id;
            SqlCommand cmd = new SqlCommand(sql, GetConnection());
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
            
        }
        public int insert_update_delete(string sql)
        {
            SqlCommand cmd = new SqlCommand(sql, GetConnection());
            int result = cmd.ExecuteNonQuery();
            return result;
        }
    }
}
