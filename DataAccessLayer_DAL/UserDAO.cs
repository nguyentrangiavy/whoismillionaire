﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
namespace DataAccessLayer_DAL
{
    public class UserDAO: GeneralDAO
    {
        public User findUserByUserName(string userName)
        {
            string sql = "UserName = '" + userName + "'";
            SqlDataReader dr = findBy("[User]", sql);
            if (dr.Read())
            {
                User user = new User()
                {
                    id = dr.GetInt32(0),
                    userName = dr.GetString(4),
                    password = dr.GetString(3),
                    name = dr.GetString(1),
                    email = dr.GetString(2),
                    role = dr.GetString(5)
                };
                return user;
            }
            return null;
        }
        public int createUser(User user)
        {
            return insert_update_delete("INSERT INTO [USER](Name,Email,Password,UserName,Role) VALUES(N'" + user.name + "',N'" + user.email + "',N'" + user.password + "',N'" + user.userName+"',N'"+user.role+"');");
        }

    }
}
