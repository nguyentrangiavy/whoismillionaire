﻿
namespace GraphicalUserInterface_GUI
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.btn_question = new System.Windows.Forms.Button();
            this.btn_answerC = new System.Windows.Forms.Button();
            this.btn_answerD = new System.Windows.Forms.Button();
            this.btn_answerB = new System.Windows.Forms.Button();
            this.btn_answerA = new System.Windows.Forms.Button();
            this.btn_reward_15 = new System.Windows.Forms.Button();
            this.btn_reward_14 = new System.Windows.Forms.Button();
            this.btn_reward_13 = new System.Windows.Forms.Button();
            this.btn_reward_12 = new System.Windows.Forms.Button();
            this.btn_reward_11 = new System.Windows.Forms.Button();
            this.btn_reward_10 = new System.Windows.Forms.Button();
            this.btn_reward_9 = new System.Windows.Forms.Button();
            this.btn_reward_8 = new System.Windows.Forms.Button();
            this.btn_reward_7 = new System.Windows.Forms.Button();
            this.btn_reward_6 = new System.Windows.Forms.Button();
            this.btn_reward_5 = new System.Windows.Forms.Button();
            this.btn_reward_4 = new System.Windows.Forms.Button();
            this.btn_reward_3 = new System.Windows.Forms.Button();
            this.btn_reward_2 = new System.Windows.Forms.Button();
            this.btn_reward_1 = new System.Windows.Forms.Button();
            this.btn_5050 = new System.Windows.Forms.Button();
            this.btn_call = new System.Windows.Forms.Button();
            this.btn_audience = new System.Windows.Forms.Button();
            this.btn_advise = new System.Windows.Forms.Button();
            this.txt_question = new System.Windows.Forms.TextBox();
            this.chart_audience = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pic_exit = new System.Windows.Forms.PictureBox();
            this.btn_advise_1 = new System.Windows.Forms.Button();
            this.btn_advise_2 = new System.Windows.Forms.Button();
            this.btn_advise_3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart_audience)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_exit)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_question
            // 
            this.btn_question.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_question.BackColor = System.Drawing.Color.Transparent;
            this.btn_question.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.question1;
            this.btn_question.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_question.FlatAppearance.BorderSize = 0;
            this.btn_question.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_question.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_question.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_question.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_question.ForeColor = System.Drawing.Color.White;
            this.btn_question.Location = new System.Drawing.Point(-84, 432);
            this.btn_question.Name = "btn_question";
            this.btn_question.Size = new System.Drawing.Size(1110, 120);
            this.btn_question.TabIndex = 0;
            this.btn_question.TabStop = false;
            this.btn_question.UseVisualStyleBackColor = false;
            // 
            // btn_answerC
            // 
            this.btn_answerC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_answerC.BackColor = System.Drawing.Color.Transparent;
            this.btn_answerC.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.question;
            this.btn_answerC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_answerC.FlatAppearance.BorderSize = 0;
            this.btn_answerC.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_answerC.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_answerC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_answerC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_answerC.ForeColor = System.Drawing.Color.White;
            this.btn_answerC.Location = new System.Drawing.Point(60, 649);
            this.btn_answerC.Name = "btn_answerC";
            this.btn_answerC.Padding = new System.Windows.Forms.Padding(15);
            this.btn_answerC.Size = new System.Drawing.Size(410, 67);
            this.btn_answerC.TabIndex = 1;
            this.btn_answerC.Text = "C. 3";
            this.btn_answerC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_answerC.UseVisualStyleBackColor = false;
            this.btn_answerC.Click += new System.EventHandler(this.btn_answerC_Click);
            // 
            // btn_answerD
            // 
            this.btn_answerD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_answerD.BackColor = System.Drawing.Color.Transparent;
            this.btn_answerD.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.question;
            this.btn_answerD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_answerD.FlatAppearance.BorderSize = 0;
            this.btn_answerD.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_answerD.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_answerD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_answerD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_answerD.ForeColor = System.Drawing.Color.White;
            this.btn_answerD.Location = new System.Drawing.Point(476, 649);
            this.btn_answerD.Name = "btn_answerD";
            this.btn_answerD.Padding = new System.Windows.Forms.Padding(15);
            this.btn_answerD.Size = new System.Drawing.Size(410, 67);
            this.btn_answerD.TabIndex = 2;
            this.btn_answerD.Text = "D. 4";
            this.btn_answerD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_answerD.UseVisualStyleBackColor = false;
            this.btn_answerD.Click += new System.EventHandler(this.btn_answerD_Click);
            // 
            // btn_answerB
            // 
            this.btn_answerB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_answerB.BackColor = System.Drawing.Color.Transparent;
            this.btn_answerB.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.question;
            this.btn_answerB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_answerB.FlatAppearance.BorderSize = 0;
            this.btn_answerB.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_answerB.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_answerB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_answerB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_answerB.ForeColor = System.Drawing.Color.White;
            this.btn_answerB.Location = new System.Drawing.Point(476, 564);
            this.btn_answerB.Name = "btn_answerB";
            this.btn_answerB.Padding = new System.Windows.Forms.Padding(15);
            this.btn_answerB.Size = new System.Drawing.Size(410, 67);
            this.btn_answerB.TabIndex = 3;
            this.btn_answerB.Text = "B. 2";
            this.btn_answerB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_answerB.UseVisualStyleBackColor = false;
            this.btn_answerB.Click += new System.EventHandler(this.btn_answerB_Click);
            // 
            // btn_answerA
            // 
            this.btn_answerA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_answerA.BackColor = System.Drawing.Color.Transparent;
            this.btn_answerA.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.question;
            this.btn_answerA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_answerA.FlatAppearance.BorderSize = 0;
            this.btn_answerA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_answerA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_answerA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_answerA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_answerA.ForeColor = System.Drawing.Color.White;
            this.btn_answerA.Location = new System.Drawing.Point(60, 564);
            this.btn_answerA.Name = "btn_answerA";
            this.btn_answerA.Padding = new System.Windows.Forms.Padding(15);
            this.btn_answerA.Size = new System.Drawing.Size(410, 67);
            this.btn_answerA.TabIndex = 4;
            this.btn_answerA.Text = "A. 1";
            this.btn_answerA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_answerA.UseVisualStyleBackColor = false;
            this.btn_answerA.Click += new System.EventHandler(this.btn_answerA_Click);
            // 
            // btn_reward_15
            // 
            this.btn_reward_15.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_15.FlatAppearance.BorderSize = 0;
            this.btn_reward_15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_15.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_15.ForeColor = System.Drawing.Color.White;
            this.btn_reward_15.Location = new System.Drawing.Point(941, 44);
            this.btn_reward_15.Name = "btn_reward_15";
            this.btn_reward_15.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_15.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_15.TabIndex = 6;
            this.btn_reward_15.TabStop = false;
            this.btn_reward_15.Text = "15     150.000.000";
            this.btn_reward_15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_15.UseVisualStyleBackColor = false;
            // 
            // btn_reward_14
            // 
            this.btn_reward_14.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_14.FlatAppearance.BorderSize = 0;
            this.btn_reward_14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_14.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_14.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_14.Location = new System.Drawing.Point(941, 90);
            this.btn_reward_14.Name = "btn_reward_14";
            this.btn_reward_14.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_14.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_14.TabIndex = 7;
            this.btn_reward_14.TabStop = false;
            this.btn_reward_14.Text = "14     85.000.000";
            this.btn_reward_14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_14.UseVisualStyleBackColor = false;
            // 
            // btn_reward_13
            // 
            this.btn_reward_13.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_13.FlatAppearance.BorderSize = 0;
            this.btn_reward_13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_13.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_13.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_13.Location = new System.Drawing.Point(941, 136);
            this.btn_reward_13.Name = "btn_reward_13";
            this.btn_reward_13.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_13.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_13.TabIndex = 8;
            this.btn_reward_13.TabStop = false;
            this.btn_reward_13.Text = "13     60.000.000";
            this.btn_reward_13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_13.UseVisualStyleBackColor = false;
            // 
            // btn_reward_12
            // 
            this.btn_reward_12.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_12.FlatAppearance.BorderSize = 0;
            this.btn_reward_12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_12.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_12.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_12.Location = new System.Drawing.Point(941, 182);
            this.btn_reward_12.Name = "btn_reward_12";
            this.btn_reward_12.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_12.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_12.TabIndex = 9;
            this.btn_reward_12.TabStop = false;
            this.btn_reward_12.Text = "12     40.000.000";
            this.btn_reward_12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_12.UseVisualStyleBackColor = false;
            // 
            // btn_reward_11
            // 
            this.btn_reward_11.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_11.FlatAppearance.BorderSize = 0;
            this.btn_reward_11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_11.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_11.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_11.Location = new System.Drawing.Point(941, 228);
            this.btn_reward_11.Name = "btn_reward_11";
            this.btn_reward_11.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_11.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_11.TabIndex = 10;
            this.btn_reward_11.TabStop = false;
            this.btn_reward_11.Text = "11     30.000.000";
            this.btn_reward_11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_11.UseVisualStyleBackColor = false;
            // 
            // btn_reward_10
            // 
            this.btn_reward_10.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_10.FlatAppearance.BorderSize = 0;
            this.btn_reward_10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_10.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_10.ForeColor = System.Drawing.Color.White;
            this.btn_reward_10.Location = new System.Drawing.Point(941, 274);
            this.btn_reward_10.Name = "btn_reward_10";
            this.btn_reward_10.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_10.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_10.TabIndex = 11;
            this.btn_reward_10.TabStop = false;
            this.btn_reward_10.Text = "10     22.000.000";
            this.btn_reward_10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_10.UseVisualStyleBackColor = false;
            // 
            // btn_reward_9
            // 
            this.btn_reward_9.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_9.FlatAppearance.BorderSize = 0;
            this.btn_reward_9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_9.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_9.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_9.Location = new System.Drawing.Point(941, 320);
            this.btn_reward_9.Name = "btn_reward_9";
            this.btn_reward_9.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_9.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_9.TabIndex = 12;
            this.btn_reward_9.TabStop = false;
            this.btn_reward_9.Text = "9       14.000.000";
            this.btn_reward_9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_9.UseVisualStyleBackColor = false;
            // 
            // btn_reward_8
            // 
            this.btn_reward_8.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_8.FlatAppearance.BorderSize = 0;
            this.btn_reward_8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_8.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_8.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_8.Location = new System.Drawing.Point(941, 366);
            this.btn_reward_8.Name = "btn_reward_8";
            this.btn_reward_8.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_8.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_8.TabIndex = 13;
            this.btn_reward_8.TabStop = false;
            this.btn_reward_8.Text = "8       10.000.000";
            this.btn_reward_8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_8.UseVisualStyleBackColor = false;
            // 
            // btn_reward_7
            // 
            this.btn_reward_7.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_7.FlatAppearance.BorderSize = 0;
            this.btn_reward_7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_7.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_7.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_7.Location = new System.Drawing.Point(941, 412);
            this.btn_reward_7.Name = "btn_reward_7";
            this.btn_reward_7.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_7.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_7.TabIndex = 14;
            this.btn_reward_7.TabStop = false;
            this.btn_reward_7.Text = "7       6.000.000";
            this.btn_reward_7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_7.UseVisualStyleBackColor = false;
            // 
            // btn_reward_6
            // 
            this.btn_reward_6.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_6.FlatAppearance.BorderSize = 0;
            this.btn_reward_6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_6.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_6.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_6.Location = new System.Drawing.Point(941, 458);
            this.btn_reward_6.Name = "btn_reward_6";
            this.btn_reward_6.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_6.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_6.TabIndex = 15;
            this.btn_reward_6.TabStop = false;
            this.btn_reward_6.Text = "6       3.000.000";
            this.btn_reward_6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_6.UseVisualStyleBackColor = false;
            // 
            // btn_reward_5
            // 
            this.btn_reward_5.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_5.FlatAppearance.BorderSize = 0;
            this.btn_reward_5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_5.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_5.ForeColor = System.Drawing.Color.White;
            this.btn_reward_5.Location = new System.Drawing.Point(941, 504);
            this.btn_reward_5.Name = "btn_reward_5";
            this.btn_reward_5.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_5.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_5.TabIndex = 16;
            this.btn_reward_5.TabStop = false;
            this.btn_reward_5.Text = "5       2.000.000";
            this.btn_reward_5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_5.UseVisualStyleBackColor = false;
            // 
            // btn_reward_4
            // 
            this.btn_reward_4.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_4.FlatAppearance.BorderSize = 0;
            this.btn_reward_4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_4.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_4.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_4.Location = new System.Drawing.Point(941, 550);
            this.btn_reward_4.Name = "btn_reward_4";
            this.btn_reward_4.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_4.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_4.TabIndex = 17;
            this.btn_reward_4.TabStop = false;
            this.btn_reward_4.Text = "4       1.000.000";
            this.btn_reward_4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_4.UseVisualStyleBackColor = false;
            // 
            // btn_reward_3
            // 
            this.btn_reward_3.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_3.FlatAppearance.BorderSize = 0;
            this.btn_reward_3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_3.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_3.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_3.Location = new System.Drawing.Point(941, 596);
            this.btn_reward_3.Name = "btn_reward_3";
            this.btn_reward_3.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_3.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_3.TabIndex = 18;
            this.btn_reward_3.TabStop = false;
            this.btn_reward_3.Text = "3       600.000";
            this.btn_reward_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_3.UseVisualStyleBackColor = false;
            // 
            // btn_reward_2
            // 
            this.btn_reward_2.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_2.FlatAppearance.BorderSize = 0;
            this.btn_reward_2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_2.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_2.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_2.Location = new System.Drawing.Point(941, 642);
            this.btn_reward_2.Name = "btn_reward_2";
            this.btn_reward_2.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_2.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_2.TabIndex = 19;
            this.btn_reward_2.TabStop = false;
            this.btn_reward_2.Text = "2       400.000";
            this.btn_reward_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_2.UseVisualStyleBackColor = false;
            // 
            // btn_reward_1
            // 
            this.btn_reward_1.BackColor = System.Drawing.Color.Transparent;
            this.btn_reward_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_reward_1.FlatAppearance.BorderSize = 0;
            this.btn_reward_1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_reward_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_reward_1.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reward_1.ForeColor = System.Drawing.Color.Orange;
            this.btn_reward_1.Location = new System.Drawing.Point(941, 688);
            this.btn_reward_1.Name = "btn_reward_1";
            this.btn_reward_1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btn_reward_1.Size = new System.Drawing.Size(247, 40);
            this.btn_reward_1.TabIndex = 20;
            this.btn_reward_1.TabStop = false;
            this.btn_reward_1.Text = "1       200.000";
            this.btn_reward_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_reward_1.UseVisualStyleBackColor = false;
            // 
            // btn_5050
            // 
            this.btn_5050.BackColor = System.Drawing.Color.Transparent;
            this.btn_5050.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources._5050;
            this.btn_5050.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_5050.FlatAppearance.BorderSize = 0;
            this.btn_5050.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_5050.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_5050.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_5050.Location = new System.Drawing.Point(46, 27);
            this.btn_5050.Name = "btn_5050";
            this.btn_5050.Size = new System.Drawing.Size(165, 84);
            this.btn_5050.TabIndex = 21;
            this.btn_5050.TabStop = false;
            this.btn_5050.UseVisualStyleBackColor = false;
            this.btn_5050.Click += new System.EventHandler(this.btn_5050_Click);
            // 
            // btn_call
            // 
            this.btn_call.BackColor = System.Drawing.Color.Transparent;
            this.btn_call.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.goidienthoainguoithan;
            this.btn_call.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_call.FlatAppearance.BorderSize = 0;
            this.btn_call.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_call.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_call.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_call.Location = new System.Drawing.Point(273, 27);
            this.btn_call.Name = "btn_call";
            this.btn_call.Size = new System.Drawing.Size(165, 84);
            this.btn_call.TabIndex = 22;
            this.btn_call.TabStop = false;
            this.btn_call.UseVisualStyleBackColor = false;
            this.btn_call.Click += new System.EventHandler(this.btn_call_Click);
            // 
            // btn_audience
            // 
            this.btn_audience.BackColor = System.Drawing.Color.Transparent;
            this.btn_audience.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.hoiykienkhangia;
            this.btn_audience.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_audience.FlatAppearance.BorderSize = 0;
            this.btn_audience.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_audience.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_audience.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_audience.Location = new System.Drawing.Point(500, 27);
            this.btn_audience.Name = "btn_audience";
            this.btn_audience.Size = new System.Drawing.Size(165, 84);
            this.btn_audience.TabIndex = 23;
            this.btn_audience.TabStop = false;
            this.btn_audience.UseVisualStyleBackColor = false;
            this.btn_audience.Click += new System.EventHandler(this.btn_audience_Click);
            // 
            // btn_advise
            // 
            this.btn_advise.BackColor = System.Drawing.Color.Transparent;
            this.btn_advise.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.tuvantaicho;
            this.btn_advise.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_advise.FlatAppearance.BorderSize = 0;
            this.btn_advise.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_advise.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_advise.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_advise.Location = new System.Drawing.Point(730, 27);
            this.btn_advise.Name = "btn_advise";
            this.btn_advise.Size = new System.Drawing.Size(165, 84);
            this.btn_advise.TabIndex = 24;
            this.btn_advise.TabStop = false;
            this.btn_advise.UseVisualStyleBackColor = false;
            this.btn_advise.Click += new System.EventHandler(this.btn_advise_Click);
            // 
            // txt_question
            // 
            this.txt_question.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(112)))), ((int)(((byte)(192)))));
            this.txt_question.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_question.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_question.ForeColor = System.Drawing.Color.White;
            this.txt_question.Location = new System.Drawing.Point(95, 445);
            this.txt_question.Multiline = true;
            this.txt_question.Name = "txt_question";
            this.txt_question.ReadOnly = true;
            this.txt_question.Size = new System.Drawing.Size(770, 96);
            this.txt_question.TabIndex = 25;
            this.txt_question.TabStop = false;
            this.txt_question.Text = "1+1=...?";
            this.txt_question.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chart_audience
            // 
            chartArea2.Name = "ChartArea1";
            this.chart_audience.ChartAreas.Add(chartArea2);
            this.chart_audience.Location = new System.Drawing.Point(311, 132);
            this.chart_audience.Name = "chart_audience";
            this.chart_audience.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chart_audience.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.DodgerBlue};
            series2.ChartArea = "ChartArea1";
            series2.Name = "percent";
            this.chart_audience.Series.Add(series2);
            this.chart_audience.Size = new System.Drawing.Size(329, 275);
            this.chart_audience.TabIndex = 26;
            title2.Alignment = System.Drawing.ContentAlignment.MiddleLeft;
            title2.Name = "Title1";
            this.chart_audience.Titles.Add(title2);
            this.chart_audience.Visible = false;
            // 
            // pic_exit
            // 
            this.pic_exit.BackColor = System.Drawing.Color.Transparent;
            this.pic_exit.Image = global::GraphicalUserInterface_GUI.Properties.Resources.delete;
            this.pic_exit.Location = new System.Drawing.Point(1163, 12);
            this.pic_exit.Name = "pic_exit";
            this.pic_exit.Size = new System.Drawing.Size(25, 30);
            this.pic_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_exit.TabIndex = 27;
            this.pic_exit.TabStop = false;
            this.pic_exit.Click += new System.EventHandler(this.pic_exit_Click);
            // 
            // btn_advise_1
            // 
            this.btn_advise_1.BackColor = System.Drawing.Color.Transparent;
            this.btn_advise_1.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.dap_an_tu_van;
            this.btn_advise_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_advise_1.FlatAppearance.BorderSize = 0;
            this.btn_advise_1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_advise_1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_advise_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_advise_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_advise_1.ForeColor = System.Drawing.Color.Orange;
            this.btn_advise_1.Location = new System.Drawing.Point(-15, 192);
            this.btn_advise_1.Name = "btn_advise_1";
            this.btn_advise_1.Padding = new System.Windows.Forms.Padding(0, 0, 40, 0);
            this.btn_advise_1.Size = new System.Drawing.Size(185, 52);
            this.btn_advise_1.TabIndex = 31;
            this.btn_advise_1.Text = "A";
            this.btn_advise_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_advise_1.UseVisualStyleBackColor = false;
            this.btn_advise_1.Visible = false;
            // 
            // btn_advise_2
            // 
            this.btn_advise_2.BackColor = System.Drawing.Color.Transparent;
            this.btn_advise_2.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.dap_an_tu_van;
            this.btn_advise_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_advise_2.FlatAppearance.BorderSize = 0;
            this.btn_advise_2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_advise_2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_advise_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_advise_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_advise_2.ForeColor = System.Drawing.Color.Orange;
            this.btn_advise_2.Location = new System.Drawing.Point(-15, 260);
            this.btn_advise_2.Name = "btn_advise_2";
            this.btn_advise_2.Padding = new System.Windows.Forms.Padding(0, 0, 40, 0);
            this.btn_advise_2.Size = new System.Drawing.Size(185, 52);
            this.btn_advise_2.TabIndex = 32;
            this.btn_advise_2.Text = "A";
            this.btn_advise_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_advise_2.UseVisualStyleBackColor = false;
            this.btn_advise_2.Visible = false;
            // 
            // btn_advise_3
            // 
            this.btn_advise_3.BackColor = System.Drawing.Color.Transparent;
            this.btn_advise_3.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.dap_an_tu_van;
            this.btn_advise_3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_advise_3.FlatAppearance.BorderSize = 0;
            this.btn_advise_3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_advise_3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_advise_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_advise_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_advise_3.ForeColor = System.Drawing.Color.Orange;
            this.btn_advise_3.Location = new System.Drawing.Point(-15, 327);
            this.btn_advise_3.Name = "btn_advise_3";
            this.btn_advise_3.Padding = new System.Windows.Forms.Padding(0, 0, 40, 0);
            this.btn_advise_3.Size = new System.Drawing.Size(185, 52);
            this.btn_advise_3.TabIndex = 33;
            this.btn_advise_3.Text = "A";
            this.btn_advise_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_advise_3.UseVisualStyleBackColor = false;
            this.btn_advise_3.Visible = false;
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GraphicalUserInterface_GUI.Properties.Resources.login_background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1200, 740);
            this.Controls.Add(this.btn_advise_3);
            this.Controls.Add(this.btn_advise_2);
            this.Controls.Add(this.btn_advise_1);
            this.Controls.Add(this.pic_exit);
            this.Controls.Add(this.chart_audience);
            this.Controls.Add(this.txt_question);
            this.Controls.Add(this.btn_advise);
            this.Controls.Add(this.btn_audience);
            this.Controls.Add(this.btn_call);
            this.Controls.Add(this.btn_5050);
            this.Controls.Add(this.btn_reward_1);
            this.Controls.Add(this.btn_reward_2);
            this.Controls.Add(this.btn_reward_3);
            this.Controls.Add(this.btn_reward_4);
            this.Controls.Add(this.btn_reward_5);
            this.Controls.Add(this.btn_reward_6);
            this.Controls.Add(this.btn_reward_7);
            this.Controls.Add(this.btn_reward_8);
            this.Controls.Add(this.btn_reward_9);
            this.Controls.Add(this.btn_reward_10);
            this.Controls.Add(this.btn_reward_11);
            this.Controls.Add(this.btn_reward_12);
            this.Controls.Add(this.btn_reward_13);
            this.Controls.Add(this.btn_reward_14);
            this.Controls.Add(this.btn_reward_15);
            this.Controls.Add(this.btn_answerA);
            this.Controls.Add(this.btn_answerB);
            this.Controls.Add(this.btn_answerD);
            this.Controls.Add(this.btn_answerC);
            this.Controls.Add(this.btn_question);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Game";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game";
            this.Load += new System.EventHandler(this.Game_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart_audience)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_exit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_question;
        private System.Windows.Forms.Button btn_answerC;
        private System.Windows.Forms.Button btn_answerD;
        private System.Windows.Forms.Button btn_answerB;
        private System.Windows.Forms.Button btn_answerA;
        private System.Windows.Forms.Button btn_reward_15;
        private System.Windows.Forms.Button btn_reward_14;
        private System.Windows.Forms.Button btn_reward_13;
        private System.Windows.Forms.Button btn_reward_12;
        private System.Windows.Forms.Button btn_reward_11;
        private System.Windows.Forms.Button btn_reward_10;
        private System.Windows.Forms.Button btn_reward_9;
        private System.Windows.Forms.Button btn_reward_8;
        private System.Windows.Forms.Button btn_reward_7;
        private System.Windows.Forms.Button btn_reward_6;
        private System.Windows.Forms.Button btn_reward_5;
        private System.Windows.Forms.Button btn_reward_4;
        private System.Windows.Forms.Button btn_reward_3;
        private System.Windows.Forms.Button btn_reward_2;
        private System.Windows.Forms.Button btn_reward_1;
        private System.Windows.Forms.Button btn_5050;
        private System.Windows.Forms.Button btn_call;
        private System.Windows.Forms.Button btn_audience;
        private System.Windows.Forms.Button btn_advise;
        private System.Windows.Forms.TextBox txt_question;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_audience;
        private System.Windows.Forms.PictureBox pic_exit;
        private System.Windows.Forms.Button btn_advise_1;
        private System.Windows.Forms.Button btn_advise_2;
        private System.Windows.Forms.Button btn_advise_3;
    }
}