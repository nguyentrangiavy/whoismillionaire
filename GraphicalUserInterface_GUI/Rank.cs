﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer_BLL;
using DataAccessLayer_DAL;
using Telerik.WinControls.UI;

namespace GraphicalUserInterface_GUI
{
    public partial class Rank : Form
    {
        RankManagement rankMrg = new RankManagement();
        Menu menu = Application.OpenForms.OfType<Menu>().FirstOrDefault();
        SignIn signin = Application.OpenForms.OfType<SignIn>().FirstOrDefault();
        public Rank()
        {
            InitializeComponent();
            loadRank();
        }
        public void loadRank()
        {
            listview_rank.Items.Clear();
            foreach(UserRank rank in rankMrg.getAll())
            {
                listview_rank.Items.Add(new ListViewItem(new string[] { rank.Rank.ToString(), rank.Name, rank.Reward.ToString().Replace("000"," 000"), rank.Time.ToString().Substring(0,19) }));
            }
        }
        private void pic_exit_Click(object sender, EventArgs e)
        {
            DialogResult exitApp = MessageBox.Show("Bạn muốn thoát game?", "Thoát", MessageBoxButtons.YesNo);
            if (exitApp == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void pic_back_Click(object sender, EventArgs e)
        {
            this.Close();
            menu.Show();
            signin.playMP3File("nhac-nen.mp3", true, 50);
        }
    }
}
