﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer_BLL;
using DataAccessLayer_DAL;

namespace GraphicalUserInterface_GUI
{
    public partial class SignUp : Form
    {
        String userNamePlacehoder = "Tên đăng nhập";
        String passwordPlacehoder = "Mật khẩu";
        String namePlacehoder = "Tên";
        String emailPlacehoder = "Email";
        UserManagement userManagement = new UserManagement();
        UserDAO userDAO = new UserDAO();
        public SignUp()
        {
            InitializeComponent();
            
        }
        public void clearFields()
        {
            txt_username.Text = userNamePlacehoder;
            txt_username.ForeColor = Color.Gray;
            txt_password.Text = passwordPlacehoder;
            txt_password.ForeColor = Color.Gray;
            txt_name.Text = namePlacehoder;
            txt_name.ForeColor = Color.Gray;
            txt_email.Text = emailPlacehoder;
            txt_email.ForeColor = Color.Gray;
        }
        public void focusTextBox(System.Windows.Forms.TextBox textBox, String placehoder)
        {
             if (textBox.Text == placehoder)
             {
                 textBox.Text = "";
                 textBox.ForeColor = Color.Black;
             }
        }
        public void outFocusTextBox(System.Windows.Forms.TextBox textBox, String placehoder)
        {
            if (String.IsNullOrWhiteSpace(textBox.Text))
            {
                textBox.Text = placehoder;
                textBox.ForeColor = Color.Gray;
            }
        }
        static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public bool checkEmptyFields()
        {
            if (txt_username.Text == userNamePlacehoder || txt_name.Text == namePlacehoder || txt_password.Text == passwordPlacehoder || txt_email.Text == emailPlacehoder)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool validatedAll()
        {
            if(valid_signup.GetError(txt_name)== "" && valid_signup.GetError(txt_email)=="" && valid_signup.GetError(txt_username)=="" && valid_signup.GetError(txt_password) == "")
            {
                return true;
            }
            return false;
        }
        private void SignUp_Load(object sender, EventArgs e)
        {
            clearFields();
        }

        private void txt_username_Enter(object sender, EventArgs e)
        {
            focusTextBox(txt_username, userNamePlacehoder);
        }

        private void txt_username_Leave(object sender, EventArgs e)
        {
            outFocusTextBox(txt_username, userNamePlacehoder);
        }

        private void txt_name_Enter(object sender, EventArgs e)
        {
            focusTextBox(txt_name, namePlacehoder);
        }

        private void txt_name_Leave(object sender, EventArgs e)
        {
            outFocusTextBox(txt_name, namePlacehoder);
        }

        private void txt_password_Enter(object sender, EventArgs e)
        {
            focusTextBox(txt_password, passwordPlacehoder);
        }

        private void txt_password_Leave(object sender, EventArgs e)
        {
            outFocusTextBox(txt_password, passwordPlacehoder);
        }

        private void txt_email_Enter(object sender, EventArgs e)
        {
            focusTextBox(txt_email, emailPlacehoder);
        }

        private void txt_email_Leave(object sender, EventArgs e)
        {
            outFocusTextBox(txt_email, emailPlacehoder);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult exitApp = MessageBox.Show("Bạn muốn thoát khỏi trò chơi?", "Thoát", MessageBoxButtons.YesNo);
            if (exitApp == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btn_signup_Click(object sender, EventArgs e)
        {
            if (validatedAll() && checkEmptyFields()==false)
            {
                User newUser = new User()
                {
                    userName = txt_username.Text,
                    name = txt_name.Text,
                    password = txt_password.Text,
                    email = txt_email.Text,
                    role = "user"
                };
                bool result = userManagement.register(newUser);
                if (result == true)
                {
                    MessageBox.Show("Đăng ký tài khoản thành công!","Đăng ký");
                    clearFields();
                    this.Hide();
                    SignIn signin = Application.OpenForms.Cast<SignIn>().FirstOrDefault();
                    signin.Show();
                }
            }
            else
            {
                ValidateChildren();
                MessageBox.Show("Vui lòng cung cấp đầy đủ và chính xác thông tin khi đăng ký!","Đăng ký");
            }
        }

        private void label_signin_Click(object sender, EventArgs e)
        {
            this.Hide();
            SignIn signin = Application.OpenForms.Cast<SignIn>().FirstOrDefault();
            signin.Show();
        }

        private void label_signin_MouseHover(object sender, EventArgs e)
        {
            label_signin.ForeColor = Color.Gray;
        }

        private void label_signin_MouseLeave(object sender, EventArgs e)
        {
            label_signin.ForeColor = Color.DodgerBlue;
        }

        private void txt_username_Validating(object sender, CancelEventArgs e)
        {
            bool checkExist = userManagement.checkExist(txt_username.Text);
            if (txt_username.Text == userNamePlacehoder)
            {
                valid_signup.SetError(txt_username, "Tên đăng nhập không được bỏ trống!");
            }
            else if (txt_username.Text.Length > 12)
            {
                valid_signup.SetError(txt_username, "Tên đăng nhập không quá 12 ký tự!");
            }
            else if (checkExist == true)
            {
                valid_signup.SetError(txt_username, "Tên đăng nhập đã được sử dụng!");
            }
            else
            {
                valid_signup.SetError(txt_username, "");
            }
        }

        private void txt_name_Validating(object sender, CancelEventArgs e)
        {
            if (txt_name.Text == namePlacehoder)
            {
                valid_signup.SetError(txt_name, "Tên người dùng không được bỏ trống!");
            }
            else if (txt_name.Text.Length > 12)
            {
                valid_signup.SetError(txt_name, "Tên người dùng không quá 12 ký tự!");
            }
            else
            {
                valid_signup.SetError(txt_name, "");
            }
        }

        private void txt_password_Validating(object sender, CancelEventArgs e)
        {
            if (txt_password.Text == passwordPlacehoder)
            {
                valid_signup.SetError(txt_password, "Mật khẩu không được bỏ trống!");
            }
            else if (txt_password.Text.Length > 12)
            {
                valid_signup.SetError(txt_password, "Mật khẩu không quá 12 ký tự!");
            }
            else
            {
                valid_signup.SetError(txt_password, "");
            }
        }

        private void txt_email_Validating(object sender, CancelEventArgs e)
        {
            bool checkMail = IsValidEmail(txt_email.Text);
            if (txt_email.Text == emailPlacehoder)
            {
                valid_signup.SetError(txt_email, "Email không được bỏ trống!");
            }
            else if (checkMail == false)
            {
                valid_signup.SetError(txt_email, "Email không hợp lệ!");
            }
            else
            {
                valid_signup.SetError(txt_email, "");
            }
        }
    }
}
