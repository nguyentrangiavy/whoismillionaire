﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalUserInterface_GUI
{
    public partial class Menu : Form
    {
        SignIn signin = Application.OpenForms.Cast<SignIn>().FirstOrDefault();

        public Menu()
        {
            InitializeComponent();
            lbl_name.Text = SignIn.name;
        }

        private void btn_play_Click(object sender, EventArgs e)
        {
            Game game = new Game();
            this.Hide();
            game.Show();
            signin.stopMP3File();
        }

        private void btn_rank_Click(object sender, EventArgs e)
        {
            Rank rank = new Rank();
            this.Hide();
            rank.Show();
            signin.playMP3File("Am-thanh-chuc-mung-chien-thang.mp3", false, 50);
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            DialogResult signOut = MessageBox.Show("Bạn thực sự muốn đăng xuất?", "Đăng xuất", MessageBoxButtons.YesNo);
            if (signOut == DialogResult.Yes)
            {
                this.Close();
                signin.Show();
                signin.playMP3File("nhac-mo-dau.mp3",true,50);
            }
        }

        private void btn_play_MouseEnter(object sender, EventArgs e)
        {
            btn_play.ForeColor = Color.Black;
        }

        private void btn_play_MouseLeave(object sender, EventArgs e)
        {
            btn_play.ForeColor = Color.White;
        }

        private void btn_rank_MouseEnter(object sender, EventArgs e)
        {
            btn_rank.ForeColor = Color.Black;
        }

        private void btn_rank_MouseLeave(object sender, EventArgs e)
        {
            btn_rank.ForeColor = Color.White;
        }

        private void btn_exit_MouseEnter(object sender, EventArgs e)
        {
            btn_play.ForeColor = Color.Black;
        }

        private void btn_exit_MouseLeave(object sender, EventArgs e)
        {
            btn_play.ForeColor = Color.White;
        }

        private void pic_exit_Click(object sender, EventArgs e)
        {
            DialogResult exitApp = MessageBox.Show("Bạn muốn thoát khỏi trò chơi?", "Thoát", MessageBoxButtons.YesNo);
            if (exitApp == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
