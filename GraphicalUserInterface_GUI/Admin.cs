﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using BussinessLogicLayer_BLL;
using DataAccessLayer_DAL;
namespace GraphicalUserInterface_GUI
{
    public partial class Admin : Telerik.WinControls.UI.RadForm
    {
        QuestionManagement quesMgr = new QuestionManagement();
        BindingSource bs = new BindingSource();
        int id = 0;
        public Admin()
        {
            InitializeComponent();
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            LoadQuestionsList();
        }
        public void LoadQuestionsList()
        {
            bs.DataSource = quesMgr.getQuestions(ddbtn_level.Text);
            gv_questions.DataSource = bs;
            gv_questions.MasterTemplate.BestFitColumns();
        }
        public bool checkEmptyFields()
        {
            if (String.IsNullOrWhiteSpace(txt_content.Text) || String.IsNullOrWhiteSpace(txt_answerA.Text) || String.IsNullOrWhiteSpace(txt_answerB.Text) || String.IsNullOrWhiteSpace(txt_answerC.Text) || String.IsNullOrWhiteSpace(txt_answerD.Text) || String.IsNullOrWhiteSpace(txt_result.Text))
            {
                return true;
            }
            return false;
        }
        public bool validatedAll()
        {
            if (valid_question.GetError(txt_content) == "" && valid_question.GetError(txt_answerA) == "" && valid_question.GetError(txt_answerB) == "" && valid_question.GetError(txt_answerC) == "" && valid_question.GetError(txt_answerD) == "" && valid_question.GetError(txt_result) == "")
            {
                return true;
            }
            return false;
        }
        public void clearAllFields()
        {
            txt_content.Text = "";
            txt_answerA.Text = "";
            txt_answerB.Text = "";
            txt_answerC.Text = "";
            txt_answerD.Text = "";
            txt_result.Text = "";
            dropdownbtn_level.Text = "Dễ";
        }
        private void btn_add_Click(object sender, EventArgs e)
        {
            if (checkEmptyFields() == false)
            {
                if (validatedAll())
                {
                    if (quesMgr.checkExistToAdd(txt_content.Text) == false)
                    {
                        Question question = new Question()
                        {
                            content = txt_content.Text,
                            answerA = txt_answerA.Text,
                            answerB = txt_answerB.Text,
                            answerC = txt_answerC.Text,
                            answerD = txt_answerD.Text,
                            result = txt_result.Text,
                            level = dropdownbtn_level.Text
                        };
                        bool result = quesMgr.addQuestion(question);
                        if (result)
                        {
                            MessageBox.Show("Thêm thành công!", "Thêm câu hỏi");
                            LoadQuestionsList();
                            bs.MoveLast();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Câu hỏi đã tồn tại!", "Thêm câu hỏi");
                    }
                }
                else
                {
                    MessageBox.Show("Dữ liệu không hợp lệ!", "Thêm câu hỏi");
                }
            }
            else
            {
                ValidateChildren();
                MessageBox.Show("Vui lòng cung cấp đầy đủ thông tin trước khi thêm câu hỏi!", "Thêm câu hỏi");
            }
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            if (checkEmptyFields() == false)
            {
                if (validatedAll())
                {
                    if(this.id != 0)
                    {
                        if (quesMgr.checkExistToUpdate(txt_content.Text, this.id) == false)
                        {
                            Question question = new Question()
                            {
                                id = this.id,
                                content = txt_content.Text,
                                answerA = txt_answerA.Text,
                                answerB = txt_answerB.Text,
                                answerC = txt_answerC.Text,
                                answerD = txt_answerD.Text,
                                result = txt_result.Text,
                                level = dropdownbtn_level.Text
                            };
                            bool result = quesMgr.updateQuestion(question);
                            if (result)
                            {
                                MessageBox.Show("Cập nhật thành công!", "Cập nhật câu hỏi");
                                LoadQuestionsList();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Câu hỏi đã tồn tại!", "Cập nhật câu hỏi");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Chọn câu hỏi trước khi cập nhật câu hỏi", "Cập nhật câu hỏi");
                    }
                }
                else
                {
                    MessageBox.Show("Dữ liệu không hợp lệ!", "Cập nhật câu hỏi");
                }

            }
            else
            {
                ValidateChildren();
                MessageBox.Show("Vui lòng cung cấp đầy đủ thông tin trước khi cập nhật câu hỏi!", "Cập nhật câu hỏi");
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (this.id != 0)
            {
                DialogResult deleteQuestion = MessageBox.Show("Bạn có thật sự muốn xóa câu hỏi này?", "Xóa câu hỏi", MessageBoxButtons.YesNo);
                if (deleteQuestion == DialogResult.Yes)
                {
                    bool result = quesMgr.deleteQuestion(this.id);
                    if (result)
                    {
                        LoadQuestionsList();
                        clearAllFields();
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng chọn câu hỏi trước khi xóa", "Xóa câu hỏi");
                    }
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn câu hỏi trước khi xóa", "Xóa câu hỏi");
            }
        }
        private void btn_exit_Click(object sender, EventArgs e)
        {
            SignIn signIn = new SignIn();
            this.Hide();
            signIn.Show();
        }

        private void mnitem_easy_Click(object sender, EventArgs e)
        {
            ddbtn_level.Text = mnitem_easy.Text;
            LoadQuestionsList();
        }

        private void mnitem_medium_Click(object sender, EventArgs e)
        {
            ddbtn_level.Text = mnitem_medium.Text;
            LoadQuestionsList();
        }

        private void mnitem_all_Click(object sender, EventArgs e)
        {
            ddbtn_level.Text = mnitem_all.Text;
            LoadQuestionsList();
        }

        private void mnitem_hard_Click(object sender, EventArgs e)
        {
            ddbtn_level.Text = mnitem_hard.Text;
            LoadQuestionsList();
        }

        private void gv_questions_CellClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var id = gv_questions.Rows[e.RowIndex].Cells[0].Value;
                var content = gv_questions.Rows[e.RowIndex].Cells[1].Value;
                var answerA = gv_questions.Rows[e.RowIndex].Cells[2].Value;
                var answerB = gv_questions.Rows[e.RowIndex].Cells[3].Value;
                var answerC = gv_questions.Rows[e.RowIndex].Cells[4].Value;
                var answerD = gv_questions.Rows[e.RowIndex].Cells[5].Value;
                var result = gv_questions.Rows[e.RowIndex].Cells[6].Value;
                var level = gv_questions.Rows[e.RowIndex].Cells[7].Value;
                this.id = Int32.Parse(id.ToString());
                txt_content.Text = content.ToString();
                txt_answerA.Text = answerA.ToString();
                txt_answerB.Text = answerB.ToString();
                txt_answerC.Text = answerC.ToString();
                txt_answerD.Text = answerD.ToString();
                txt_result.Text = result.ToString();
                dropdownbtn_level.Text = level.ToString();
            }
        }

        private void menuitem_easy_Click(object sender, EventArgs e)
        {
            dropdownbtn_level.Text = menuitem_easy.Text;
        }

        private void menuitem_medium_Click(object sender, EventArgs e)
        {
            dropdownbtn_level.Text = menuitem_medium.Text;
        }

        private void menuitem_hard_Click(object sender, EventArgs e)
        {
            dropdownbtn_level.Text = menuitem_hard.Text;
        }

        private void txt_content_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txt_content.Text))
            {
                valid_question.SetError(txt_content, "Nội dung câu hỏi không được bỏ trống!");
            }
            else if (txt_content.Text.Length > 200)
            {
                valid_question.SetError(txt_content, "Nội dung câu hỏi không quá 200 ký tự!");
            }
            else
            {
                valid_question.SetError(txt_content, "");
            }
        }

        private void txt_answerA_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txt_answerA.Text))
            {
                valid_question.SetError(txt_answerA, "Câu trả lời không được bỏ trống!");
            }
            else if (txt_answerA.Text.Length > 30)
            {
                valid_question.SetError(txt_answerA, "Câu trả lời không quá 30 ký tự!");
            }
            else
            {
                valid_question.SetError(txt_answerA, "");
            }
        }

        private void txt_answerB_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txt_answerB.Text))
            {
                valid_question.SetError(txt_answerB, "Câu trả lời không được bỏ trống!");
            }
            else if (txt_answerB.Text.Length > 30)
            {
                valid_question.SetError(txt_answerB, "Câu trả lời không quá 30 ký tự!");
            }
            else
            {
                valid_question.SetError(txt_answerB, "");
            }
        }

        private void txt_answerC_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txt_answerC.Text))
            {
                valid_question.SetError(txt_answerC, "Câu trả lời không được bỏ trống!");
            }
            else if (txt_answerC.Text.Length > 30)
            {
                valid_question.SetError(txt_answerC, "Câu trả lời không quá 30 ký tự!");
            }
            else
            {
                valid_question.SetError(txt_answerC, "");
            }
        }

        private void txt_answerD_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txt_answerD.Text))
            {
                valid_question.SetError(txt_answerD, "Câu trả lời không được bỏ trống!");
            }
            else if (txt_answerD.Text.Length > 30)
            {
                valid_question.SetError(txt_answerD, "Câu trả lời không quá 30 ký tự!");
            }
            else
            {
                valid_question.SetError(txt_answerD, "");
            }
        }

        private void txt_result_Validating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txt_result.Text))
            {
                valid_question.SetError(txt_result, "Đáp án không được bỏ trống!");
            }
            else if (txt_result.Text.Length > 30)
            {
                valid_question.SetError(txt_result, "Đáp án không quá 30 ký tự!");
            }
            else
            {
                valid_question.SetError(txt_result, "");
            }
        }

        private void pic_exit_Click(object sender, EventArgs e)
        {
            DialogResult exitApp = MessageBox.Show("Bạn có muốn thoát khỏi chương trình?", "Thoát", MessageBoxButtons.YesNo);
            if (exitApp == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
