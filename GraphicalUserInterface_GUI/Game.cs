﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer_BLL;
using DataAccessLayer_DAL;
using WMPLib;

namespace GraphicalUserInterface_GUI
{
    public partial class Game : Form
    {
        QuestionManagement quesMrg = new QuestionManagement();
        RankManagement rankMrg = new RankManagement();
        WindowsMediaPlayer player = new WindowsMediaPlayer();
        List<Question> list = new List<Question>();
        Random random = new Random();
        int questionNumber;
        decimal reward;
        Image defaultAnswerBackground = Properties.Resources.question;
        Image selectedAnswerBackground = Properties.Resources.chosen_answer;
        Image correctAnswerBackground = Properties.Resources.correct_answer;
        Image wrongAnswerBackground = Properties.Resources.wrong_answer;
        Image rewardBackground = Properties.Resources.reward;
        Menu menu = Application.OpenForms.OfType<Menu>().FirstOrDefault();
        SignIn signin = Application.OpenForms.OfType<SignIn>().FirstOrDefault();
        public Game()
        {
            InitializeComponent();
        }
        private void Game_Load(object sender, EventArgs e)
        {
            load();
        }
        public void load()
        {
            this.questionNumber = 0;
            list = quesMrg.getRandomQuestions();
            loadQuestion();
            resetSupport();
        }
        public void loadQuestion()
        {
            if(questionNumber < 15)
            {
                txt_question.Text = list[questionNumber].content;
                btn_answerA.Text = "A. " + list[questionNumber].answerA;
                btn_answerB.Text = "B. " + list[questionNumber].answerB;
                btn_answerC.Text = "C. " + list[questionNumber].answerC;
                btn_answerD.Text = "D. " + list[questionNumber].answerD;
                btn_answerA.Enabled = true;
                btn_answerB.Enabled = true;
                btn_answerC.Enabled = true;
                btn_answerD.Enabled = true;
                chart_audience.Visible = false;
                btn_advise_1.Visible = false;
                btn_advise_2.Visible = false;
                btn_advise_3.Visible = false;
                if (questionNumber == 5)
                {
                    btn_advise.Visible = true;
                }
            }
            else
            {
                getReward(false);
            }
        }
        public void clearReward()
        {
            if (this.questionNumber > 0)
            {
                switch (this.questionNumber)
                {
                    case 1:
                        btn_reward_1.BackgroundImage = null;
                        btn_reward_1.ForeColor = Color.Orange;
                        break;
                    case 2:
                        btn_reward_2.BackgroundImage = null;
                        btn_reward_2.ForeColor = Color.Orange;
                        break;
                    case 3:
                        btn_reward_3.BackgroundImage = null;
                        btn_reward_3.ForeColor = Color.Orange;
                        break;
                    case 4:
                        btn_reward_4.BackgroundImage = null;
                        btn_reward_4.ForeColor = Color.Orange;
                        break;
                    case 5:
                        btn_reward_5.BackgroundImage = null;
                        btn_reward_5.ForeColor = Color.White;
                        break;
                    case 6:
                        btn_reward_6.BackgroundImage = null;
                        btn_reward_6.ForeColor = Color.Orange;
                        break;
                    case 7:
                        btn_reward_7.BackgroundImage = null;
                        btn_reward_7.ForeColor = Color.Orange;
                        break;
                    case 8:
                        btn_reward_8.BackgroundImage = null;
                        btn_reward_8.ForeColor = Color.Orange;
                        break;
                    case 9:
                        btn_reward_9.BackgroundImage = null;
                        btn_reward_9.ForeColor = Color.Orange;
                        break;
                    case 10:
                        btn_reward_10.BackgroundImage = null;
                        btn_reward_10.ForeColor = Color.White;
                        break;
                    case 11:
                        btn_reward_11.BackgroundImage = null;
                        btn_reward_11.ForeColor = Color.Orange;
                        break;
                    case 12:
                        btn_reward_12.BackgroundImage = null;
                        btn_reward_12.ForeColor = Color.Orange;
                        break;
                    case 13:
                        btn_reward_13.BackgroundImage = null;
                        btn_reward_13.ForeColor = Color.Orange;
                        break;
                    case 14:
                        btn_reward_14.BackgroundImage = null;
                        btn_reward_14.ForeColor = Color.Orange;
                        break;
                    case 15:
                        btn_reward_15.BackgroundImage = null;
                        btn_reward_15.ForeColor = Color.White;
                        break;
                    default:
                        // code block
                        break;
                }

            }
        }
        public void loadReward()
        {
            if (this.questionNumber > 0)
            {
                switch (this.questionNumber)
                {
                    case 1:
                        btn_reward_1.BackgroundImage = rewardBackground;
                        btn_reward_1.ForeColor = Color.Black;
                        reward = 200000;
                        break;
                    case 2:
                        btn_reward_2.BackgroundImage = rewardBackground;
                        btn_reward_2.ForeColor = Color.Black;
                        reward = 400000;
                        break;
                    case 3:
                        btn_reward_3.BackgroundImage = rewardBackground;
                        btn_reward_3.ForeColor = Color.Black;
                        reward = 600000;
                        break;
                    case 4:
                        btn_reward_4.BackgroundImage = rewardBackground;
                        btn_reward_4.ForeColor = Color.Black;
                        reward = 1000000;
                        break;
                    case 5:
                        btn_reward_5.BackgroundImage = rewardBackground;
                        reward = 2000000;
                        break;
                    case 6:
                        btn_reward_6.BackgroundImage = rewardBackground;
                        btn_reward_6.ForeColor = Color.Black;
                        reward = 3000000;
                        break;
                    case 7:
                        btn_reward_7.BackgroundImage = rewardBackground;
                        btn_reward_7.ForeColor = Color.Black;
                        reward = 6000000;
                        break;
                    case 8:
                        btn_reward_8.BackgroundImage = rewardBackground;
                        btn_reward_8.ForeColor = Color.Black;
                        reward = 10000000;
                        break;
                    case 9:
                        btn_reward_9.BackgroundImage = rewardBackground;
                        btn_reward_9.ForeColor = Color.Black;
                        reward = 14000000;
                        break;
                    case 10:
                        btn_reward_10.BackgroundImage = rewardBackground;
                        reward = 22000000;
                        break;
                    case 11:
                        btn_reward_11.BackgroundImage = rewardBackground;
                        btn_reward_11.ForeColor = Color.Black;
                        reward = 30000000;
                        break;
                    case 12:
                        btn_reward_12.BackgroundImage = rewardBackground;
                        btn_reward_12.ForeColor = Color.Black;
                        reward = 40000000;
                        break;
                    case 13:
                        btn_reward_13.BackgroundImage = rewardBackground;
                        btn_reward_13.ForeColor = Color.Black;
                        reward = 60000000;
                        break;
                    case 14:
                        btn_reward_14.BackgroundImage = rewardBackground;
                        btn_reward_14.ForeColor = Color.Black;
                        reward = 85000000;
                        break;
                    case 15:
                        btn_reward_15.BackgroundImage = rewardBackground;
                        reward = 150000000;
                        break;
                    default:
                        // code block
                        break;
                }

            }
        }
        public void ranking()
        {
            DataAccessLayer_DAL.Rank rank = new DataAccessLayer_DAL.Rank()
            {
                reward = reward,
                date = DateTime.Now,
                userId = SignIn.id
            };
            if (rankMrg.checkExist(SignIn.id))
            {
                
                if (rankMrg.checkReward(reward,SignIn.id))
                {
                    rankMrg.updateRank(rank);
                }
            }
            else
            {
                rankMrg.addRank(rank);
            }
        }
        public void getReward(bool stop)
        {
            
            if (stop)
            {
                MessageBox.Show("Bạn đã dừng cuộc chơi!" + System.Environment.NewLine + "Giải thưởng của bạn là: " + reward.ToString().Replace("000"," 000")+ " VNĐ");
                ranking();
            }
            else
            {
                if(questionNumber >= 15)
                {
                    reward = 150000000;
                    MessageBox.Show("Chúc mừng bạn đã chiến thắng!" + System.Environment.NewLine + "Giải thưởng của bạn là: " + reward.ToString().Replace("000", " 000") + " VNĐ");
                }
                else
                {
                    if (questionNumber >= 0 && questionNumber < 5)
                    {
                        reward = 0;
                    }
                    else if (questionNumber >= 5 && questionNumber < 10)
                    {
                        reward = 2000000;
                    }
                    else if (questionNumber >= 10 && questionNumber < 15)
                    {
                        reward = 22000000;
                    }
                    MessageBox.Show("Bạn đã trả lời sai! Rất tiếc bạn phải dừng cuộc chơi ở đây!" + System.Environment.NewLine + "Giải thưởng của bạn là: " + reward.ToString().Replace("000", " 000") + " VNĐ");
                }
                ranking();
            }
        }
        public void resetSupport()
        {
            btn_5050.Enabled = true;
            btn_5050.BackgroundImage = Properties.Resources._5050;
            btn_call.Enabled = true;
            btn_call.BackgroundImage = Properties.Resources.goidienthoainguoithan;
            btn_audience.Enabled = true;
            btn_audience.BackgroundImage = Properties.Resources.hoiykienkhangia;
            btn_advise.Enabled = true;
            btn_advise.BackgroundImage = Properties.Resources.tuvantaicho;
            btn_advise.Visible = false;
        }
        public void afterCorrectAnswer()
        {
            clearReward();
            this.questionNumber++;
            if (this.questionNumber % 5 == 0)
            {
                signin.playMP3File("Nhac-tra-loi-dung.mp3", false, 70);
            }
            else
            {
                signin.playMP3File("Am-thanh-tra-loi-dung.mp3", false, 50);
            }
            MessageBox.Show("Câu trả lời của bạn là câu trả lời hoàn toàn chính xác!", "Trả lời đúng");
            loadQuestion();
            loadReward();
        }
        public void afterWrongAnswer()
        {
            signin.playMP3File("Am-thanh-tra-loi-sai.mp3", false, 70);
            getReward(false);
            DialogResult replay =  MessageBox.Show(" Bạn có muốn chơi lại?", "Trả lời sai",MessageBoxButtons.YesNo);
            if(replay == DialogResult.Yes)
            {
                clearReward();
                load();
            }
            else
            {
                this.Close();
                menu.Show();
            }
        }
        async Task getSuggestion(string suggestion)
        {
            signin.playMP3File("goi-dien-thoai-nguoi-than.mp3", false, 70);
            await Task.Delay(1000);
            MessageBox.Show("Má bạn đã gợi ý cho bạn chọn đáp án " + suggestion, "Quyền trợ giúp gọi điện thoại cho người thân");
        }
        async Task getAdvise(String first, String second, String third)
        {
            await Task.Delay(1000);
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            btn_advise_1.Text = first;
            btn_advise_1.Visible = true;
            await Task.Delay(1000);
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            btn_advise_2.Text = second;
            btn_advise_2.Visible = true;
            await Task.Delay(1000);
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            btn_advise_3.Text = third;
            btn_advise_3.Visible = true;
        }
        public void getChart(List<string> answers)
        {
            chart_audience.Series["percent"].Points.Clear();
            chart_audience.Visible = true;
            int percentA = 0;
            int percentB = 0;
            int percentC = 0;
            int percentD = 0;
            if (answers.Find(x => x == "A") == "A" && answers.Find(x => x == "B") == "B")
            {
                percentA = random.Next(0, 100);
                percentB = random.Next(0, 100 - percentA);
            }
            else if (answers.Find(x => x == "A") == "A" && answers.Find(x => x == "C") == "C")
            {
                percentA = random.Next(0, 100);
                percentC = random.Next(0, 100 - percentA);
            }
            else if (answers.Find(x => x == "A") == "A" && answers.Find(x => x == "D") == "D")
            {
                percentA = random.Next(0, 100);
                percentD = random.Next(0, 100 - percentA);
            }
            else if (answers.Find(x => x == "B") == "B" && answers.Find(x => x == "C") == "C")
            {
                percentB = random.Next(0, 100);
                percentC = random.Next(0, 100 - percentB);
            }
            else if(answers.Find(x => x == "B") == "B" && answers.Find(x => x == "D") == "D")
            {
                percentB = random.Next(0, 100);
                percentD = random.Next(0, 100 - percentB);
            }
            else if(answers.Find(x => x == "C") == "C" && answers.Find(x => x == "D") == "D")
            {
                percentC = random.Next(0, 100);
                percentD = random.Next(0, 100 - percentC);
            }
            else
            {
                percentA = random.Next(0, 100);
                percentB = random.Next(0, 100 - percentA);
                percentC = random.Next(0, 100 - (percentA + percentB));
                percentD = 100 - (percentA + percentB + percentC);
            }
            chart_audience.Series["percent"].Points.AddXY("A",percentA.ToString());
            chart_audience.Series["percent"].Points.AddXY("B", percentB.ToString());
            chart_audience.Series["percent"].Points.AddXY("C",percentC.ToString());
            chart_audience.Series["percent"].Points.AddXY("D",percentD.ToString());
            chart_audience.Titles[0].Text = "                   " + percentA.ToString() + "%    " + percentB.ToString() + "%    " + percentC.ToString() + "%     " + percentD.ToString() + "%";
        }
        public List<string> getEnabledAnswers()
        {
            List<string> answers = new List<string>();
            if (btn_answerA.Text != "")
            {
                answers.Add("A");
            }
            if (btn_answerB.Text != "")
            {
                answers.Add("B");
            }
            if (btn_answerC.Text != "")
            {
                answers.Add("C");
            }
            if (btn_answerD.Text != "")
            {
                answers.Add("D");
            }
            return answers;
        }
        private void btn_answerA_Click(object sender, EventArgs e)
        {
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            btn_answerA.BackgroundImage = selectedAnswerBackground;
            DialogResult conformAnswer = MessageBox.Show("Đáp án cuối cùng của bạn là "+btn_answerA.Text +"?", "Trả lời", MessageBoxButtons.YesNo);
            if (conformAnswer == DialogResult.Yes)
            {
                if (list[questionNumber].answerA == list[questionNumber].result)
                {
                    btn_answerA.BackgroundImage = correctAnswerBackground;
                    afterCorrectAnswer();
                    btn_answerA.BackgroundImage = defaultAnswerBackground;
                }
                else
                {
                    btn_answerA.BackgroundImage = wrongAnswerBackground;
                    afterWrongAnswer();
                    btn_answerA.BackgroundImage = defaultAnswerBackground;
                }
            }
            else
            {
                btn_answerA.BackgroundImage = defaultAnswerBackground;
            }
        }

        private void btn_answerB_Click(object sender, EventArgs e)
        {
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            btn_answerB.BackgroundImage = selectedAnswerBackground;
            DialogResult conformAnswer = MessageBox.Show("Đáp án cuối cùng của bạn là " + btn_answerB.Text + "?", "Trả lời", MessageBoxButtons.YesNo);
            if (conformAnswer == DialogResult.Yes)
            {
                if (list[questionNumber].answerB == list[questionNumber].result)
                {
                    btn_answerB.BackgroundImage = correctAnswerBackground;
                    afterCorrectAnswer();
                    btn_answerB.BackgroundImage = defaultAnswerBackground;
                }
                else
                {
                    btn_answerB.BackgroundImage = wrongAnswerBackground;
                    afterWrongAnswer();
                    btn_answerB.BackgroundImage = defaultAnswerBackground;
                }
            }
            else
            {
                btn_answerB.BackgroundImage = defaultAnswerBackground;
            }
        }

        private void btn_answerC_Click(object sender, EventArgs e)
        {
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            btn_answerC.BackgroundImage = selectedAnswerBackground;
            DialogResult conformAnswer = MessageBox.Show("Đáp án cuối cùng của bạn là " + btn_answerC.Text + "?", "Trả lời", MessageBoxButtons.YesNo);
            if (conformAnswer == DialogResult.Yes)
            {
                if (list[questionNumber].answerC == list[questionNumber].result)
                {
                    btn_answerC.BackgroundImage = correctAnswerBackground;
                    afterCorrectAnswer();
                    btn_answerC.BackgroundImage = defaultAnswerBackground;
                }
                else
                {
                    btn_answerC.BackgroundImage = wrongAnswerBackground;
                    afterWrongAnswer();
                    btn_answerC.BackgroundImage = defaultAnswerBackground;
                }
            }
            else
            {
                btn_answerC.BackgroundImage = defaultAnswerBackground;
            }
        }

        private void btn_answerD_Click(object sender, EventArgs e)
        {
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            btn_answerD.BackgroundImage = selectedAnswerBackground;
            DialogResult conformAnswer = MessageBox.Show("Đáp án cuối cùng của bạn là " + btn_answerD.Text + "?", "Trả lời", MessageBoxButtons.YesNo);
            if (conformAnswer == DialogResult.Yes)
            {
                if (list[questionNumber].answerD == list[questionNumber].result)
                {
                    btn_answerD.BackgroundImage = correctAnswerBackground;
                    afterCorrectAnswer();
                    btn_answerD.BackgroundImage = defaultAnswerBackground;
                }
                else
                {
                    btn_answerD.BackgroundImage = wrongAnswerBackground;
                    afterWrongAnswer();
                    btn_answerD.BackgroundImage = defaultAnswerBackground;
                }
            }
            else
            {
                btn_answerD.BackgroundImage = defaultAnswerBackground;
            }
        }

        private void btn_5050_Click(object sender, EventArgs e)
        {
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            DialogResult conform = MessageBox.Show("Bạn có chắc sử dụng quyền trợ giúp 50:50 không?", "Quyền trợ giúp 50:50", MessageBoxButtons.YesNo);
            if(conform == DialogResult.Yes)
            {
                btn_5050.BackgroundImage = Properties.Resources._5050_off;
                if (list[questionNumber].answerA == list[questionNumber].result)
                {
                    List<string> answers = new List<string>() { "B", "C", "D" };
                    int index = random.Next(0, answers.Count);
                    switch (answers[index])
                    {
                        case "B":
                            btn_answerC.Enabled = false;
                            btn_answerC.Text = "";
                            btn_answerD.Enabled = false;
                            btn_answerD.Text = "";
                            break;
                        case "C":
                            btn_answerB.Enabled = false;
                            btn_answerB.Text = "";
                            btn_answerD.Enabled = false;
                            btn_answerD.Text = "";
                            break;
                        case "D":
                            btn_answerB.Enabled = false;
                            btn_answerB.Text = "";
                            btn_answerC.Enabled = false;
                            btn_answerC.Text = "";
                            break;
                        default:
                            break;
                    }
                }
                else if (list[questionNumber].answerB == list[questionNumber].result)
                {
                    List<string> answers = new List<string>() { "A", "C", "D" };
                    int index = random.Next(0, answers.Count);
                    switch (answers[index])
                    {
                        case "A":
                            btn_answerC.Enabled = false;
                            btn_answerC.Text = "";
                            btn_answerD.Enabled = false;
                            btn_answerD.Text = "";
                            break;
                        case "C":
                            btn_answerA.Enabled = false;
                            btn_answerA.Text = "";
                            btn_answerD.Enabled = false;
                            btn_answerD.Text = "";
                            break;
                        case "D":
                            btn_answerA.Enabled = false;
                            btn_answerA.Text = "";
                            btn_answerC.Enabled = false;
                            btn_answerC.Text = "";
                            break;
                        default:
                            break;
                    }
                }
                else if (list[questionNumber].answerC == list[questionNumber].result)
                {
                    List<string> answers = new List<string>() { "A", "B", "D" };
                    int index = random.Next(0, answers.Count);
                    switch (answers[index])
                    {
                        case "A":
                            btn_answerB.Enabled = false;
                            btn_answerB.Text = "";
                            btn_answerD.Enabled = false;
                            btn_answerD.Text = "";
                            break;
                        case "B":
                            btn_answerA.Enabled = false;
                            btn_answerA.Text = "";
                            btn_answerD.Enabled = false;
                            btn_answerD.Text = "";
                            break;
                        case "D":
                            btn_answerA.Enabled = false;
                            btn_answerA.Text = "";
                            btn_answerB.Enabled = false;
                            btn_answerB.Text = "";
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    List<string> answers = new List<string>() { "A", "B", "C" };
                    int index = random.Next(0, answers.Count);
                    switch (answers[index])
                    {
                        case "A":
                            btn_answerB.Enabled = false;
                            btn_answerB.Text = "";
                            btn_answerC.Enabled = false;
                            btn_answerC.Text = "";
                            break;
                        case "B":
                            btn_answerA.Enabled = false;
                            btn_answerA.Text = "";
                            btn_answerC.Enabled = false;
                            btn_answerC.Text = "";
                            break;
                        case "C":
                            btn_answerA.Enabled = false;
                            btn_answerA.Text = "";
                            btn_answerB.Enabled = false;
                            btn_answerB.Text = "";
                            break;
                        default:
                            break;
                    }
                }
                btn_5050.Enabled = false;
            }
        }

        private async void btn_call_Click(object sender, EventArgs e)
        {
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            DialogResult conform = MessageBox.Show("Bạn có chắc sử dụng quyền trợ giúp gọi điện thoại cho người thân không?", "Quyền trợ giúp gọi điện thoại cho người thân", MessageBoxButtons.YesNo);
            if (conform == DialogResult.Yes)
            {
                btn_call.BackgroundImage = Properties.Resources.goidienthoainguoithan_off;
                btn_call.Enabled = false;
                List<string> answers = getEnabledAnswers();
                int index = random.Next(answers.Count);
                string suggestion = "";
                switch (answers[index])
                {
                    case "A":
                        suggestion = btn_answerA.Text;
                        break;
                    case "B":
                        suggestion = btn_answerB.Text;
                        break;
                    case "C":
                        suggestion = btn_answerC.Text;
                        break;
                    case "D":
                        suggestion = btn_answerD.Text;
                        break;
                    default:
                        break;
                }
                await getSuggestion(suggestion);
            }
        }

        private void btn_audience_Click(object sender, EventArgs e)
        {
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            DialogResult conform = MessageBox.Show("Bạn có chắc sử dụng quyền trợ giúp lấy ý kiến khán giả tại trường quay không?", "Quyền trợ giúp gọi điện thoại cho người thân", MessageBoxButtons.YesNo);
            if (conform == DialogResult.Yes)
            {
                btn_audience.Enabled = false;
                btn_audience.BackgroundImage = Properties.Resources.hoiykienkhangia_off;
                List<string> answers = getEnabledAnswers();
                getChart(answers);
            }
               
        }

        private async void btn_advise_Click(object sender, EventArgs e)
        {
            signin.playMP3File("Tieng-ting.mp3", false, 50);
            DialogResult conform = MessageBox.Show("Bạn có chắc sử dụng quyền trợ giúp hỏi tổ tư vấn tại chỗ không?", "Quyền trợ giúp gọi điện thoại cho người thân", MessageBoxButtons.YesNo);
            if (conform == DialogResult.Yes)
            {
                btn_advise.Enabled = false;
                btn_advise.BackgroundImage = Properties.Resources.tuvantaicho_off;
                List<string> answers = getEnabledAnswers();
                int first = random.Next(answers.Count);
                int second = random.Next(answers.Count);
                int third = random.Next(answers.Count);
                await getAdvise(answers[first],answers[second],answers[third]);
            }
        }

        private void pic_exit_Click(object sender, EventArgs e)
        {
            DialogResult exitApp = MessageBox.Show("Bạn muốn dừng cuộc chơi ngay bây giờ?", "Thoát", MessageBoxButtons.YesNo);
            if (exitApp == DialogResult.Yes)
            {
                getReward(true);
                this.Close();
                menu.Show();
                signin.playMP3File("Nhac-nen.mp3", false, 50);
            }
        }
    }
}
