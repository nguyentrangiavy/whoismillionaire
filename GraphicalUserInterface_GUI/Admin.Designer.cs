﻿namespace GraphicalUserInterface_GUI
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.gv_questions = new Telerik.WinControls.UI.RadGridView();
            this.aquaTheme1 = new Telerik.WinControls.Themes.AquaTheme();
            this.telerikMetroTheme1 = new Telerik.WinControls.Themes.TelerikMetroTheme();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.txt_result = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.txt_answerD = new Telerik.WinControls.UI.RadTextBox();
            this.txt_answerC = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.txt_answerB = new Telerik.WinControls.UI.RadTextBox();
            this.txt_answerA = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txt_content = new Telerik.WinControls.UI.RadTextBox();
            this.btn_add = new Telerik.WinControls.UI.RadButton();
            this.btn_update = new Telerik.WinControls.UI.RadButton();
            this.btn_delete = new Telerik.WinControls.UI.RadButton();
            this.btn_exit = new Telerik.WinControls.UI.RadButton();
            this.ddbtn_level = new Telerik.WinControls.UI.RadDropDownButton();
            this.mnitem_easy = new Telerik.WinControls.UI.RadMenuItem();
            this.mnitem_medium = new Telerik.WinControls.UI.RadMenuItem();
            this.mnitem_hard = new Telerik.WinControls.UI.RadMenuItem();
            this.mnitem_all = new Telerik.WinControls.UI.RadMenuItem();
            this.valid_question = new System.Windows.Forms.ErrorProvider(this.components);
            this.dropdownbtn_level = new Telerik.WinControls.UI.RadDropDownButton();
            this.menuitem_easy = new Telerik.WinControls.UI.RadMenuItem();
            this.menuitem_medium = new Telerik.WinControls.UI.RadMenuItem();
            this.menuitem_hard = new Telerik.WinControls.UI.RadMenuItem();
            this.object_90f6713b_1cad_4883_ae3a_3486a1328c73 = new Telerik.WinControls.RootRadElement();
            this.pic_exit = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_questions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_questions.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_result)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_answerD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_answerC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_answerB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_answerA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_content)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_update)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddbtn_level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.valid_question)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropdownbtn_level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_exit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel9
            // 
            this.radLabel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel9.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold);
            this.radLabel9.Location = new System.Drawing.Point(295, 12);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(635, 56);
            this.radLabel9.TabIndex = 3;
            this.radLabel9.Text = "QUESTIONS MANAGEMENT";
            this.radLabel9.ThemeName = "TelerikMetro";
            // 
            // gv_questions
            // 
            this.gv_questions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gv_questions.Location = new System.Drawing.Point(38, 74);
            // 
            // 
            // 
            this.gv_questions.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.gv_questions.MasterTemplate.AllowAddNewRow = false;
            this.gv_questions.MasterTemplate.AllowColumnChooser = false;
            this.gv_questions.MasterTemplate.AllowColumnHeaderContextMenu = false;
            this.gv_questions.MasterTemplate.AllowDeleteRow = false;
            this.gv_questions.MasterTemplate.AllowEditRow = false;
            this.gv_questions.MasterTemplate.AllowSearchRow = true;
            this.gv_questions.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gv_questions.Name = "gv_questions";
            this.gv_questions.Size = new System.Drawing.Size(1127, 324);
            this.gv_questions.TabIndex = 17;
            this.gv_questions.ThemeName = "TelerikMetro";
            this.gv_questions.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gv_questions_CellClick);
            // 
            // radLabel7
            // 
            this.radLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel7.Font = new System.Drawing.Font("Verdana", 12F);
            this.radLabel7.Location = new System.Drawing.Point(965, 482);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(58, 23);
            this.radLabel7.TabIndex = 37;
            this.radLabel7.Text = "Level:";
            this.radLabel7.ThemeName = "TelerikMetro";
            // 
            // radLabel8
            // 
            this.radLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel8.Font = new System.Drawing.Font("Verdana", 12F);
            this.radLabel8.Location = new System.Drawing.Point(965, 404);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(67, 23);
            this.radLabel8.TabIndex = 35;
            this.radLabel8.Text = "Result:";
            this.radLabel8.ThemeName = "TelerikMetro";
            // 
            // txt_result
            // 
            this.txt_result.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_result.Font = new System.Drawing.Font("Verdana", 12F);
            this.txt_result.Location = new System.Drawing.Point(965, 438);
            this.txt_result.Name = "txt_result";
            this.txt_result.Size = new System.Drawing.Size(200, 38);
            this.txt_result.TabIndex = 34;
            this.txt_result.ThemeName = "TelerikMetro";
            this.txt_result.Validating += new System.ComponentModel.CancelEventHandler(this.txt_result_Validating);
            // 
            // radLabel5
            // 
            this.radLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel5.Font = new System.Drawing.Font("Verdana", 12F);
            this.radLabel5.Location = new System.Drawing.Point(705, 482);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(94, 23);
            this.radLabel5.TabIndex = 33;
            this.radLabel5.Text = "Answer D:";
            this.radLabel5.ThemeName = "TelerikMetro";
            // 
            // radLabel6
            // 
            this.radLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel6.Font = new System.Drawing.Font("Verdana", 12F);
            this.radLabel6.Location = new System.Drawing.Point(442, 482);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(93, 23);
            this.radLabel6.TabIndex = 31;
            this.radLabel6.Text = "Answer C:";
            this.radLabel6.ThemeName = "TelerikMetro";
            // 
            // txt_answerD
            // 
            this.txt_answerD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_answerD.Font = new System.Drawing.Font("Verdana", 12F);
            this.txt_answerD.Location = new System.Drawing.Point(707, 516);
            this.txt_answerD.Name = "txt_answerD";
            this.txt_answerD.Size = new System.Drawing.Size(200, 38);
            this.txt_answerD.TabIndex = 32;
            this.txt_answerD.ThemeName = "TelerikMetro";
            this.txt_answerD.Validating += new System.ComponentModel.CancelEventHandler(this.txt_answerD_Validating);
            // 
            // txt_answerC
            // 
            this.txt_answerC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_answerC.Font = new System.Drawing.Font("Verdana", 12F);
            this.txt_answerC.Location = new System.Drawing.Point(444, 516);
            this.txt_answerC.Name = "txt_answerC";
            this.txt_answerC.Size = new System.Drawing.Size(200, 38);
            this.txt_answerC.TabIndex = 30;
            this.txt_answerC.ThemeName = "TelerikMetro";
            this.txt_answerC.Validating += new System.ComponentModel.CancelEventHandler(this.txt_answerC_Validating);
            // 
            // radLabel3
            // 
            this.radLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel3.Font = new System.Drawing.Font("Verdana", 12F);
            this.radLabel3.Location = new System.Drawing.Point(703, 404);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(93, 23);
            this.radLabel3.TabIndex = 29;
            this.radLabel3.Text = "Answer B:";
            this.radLabel3.ThemeName = "TelerikMetro";
            // 
            // radLabel2
            // 
            this.radLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel2.Font = new System.Drawing.Font("Verdana", 12F);
            this.radLabel2.Location = new System.Drawing.Point(440, 404);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(93, 23);
            this.radLabel2.TabIndex = 25;
            this.radLabel2.Text = "Answer A:";
            this.radLabel2.ThemeName = "TelerikMetro";
            // 
            // txt_answerB
            // 
            this.txt_answerB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_answerB.Font = new System.Drawing.Font("Verdana", 12F);
            this.txt_answerB.Location = new System.Drawing.Point(705, 438);
            this.txt_answerB.Name = "txt_answerB";
            this.txt_answerB.Size = new System.Drawing.Size(200, 38);
            this.txt_answerB.TabIndex = 28;
            this.txt_answerB.ThemeName = "TelerikMetro";
            this.txt_answerB.Validating += new System.ComponentModel.CancelEventHandler(this.txt_answerB_Validating);
            // 
            // txt_answerA
            // 
            this.txt_answerA.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_answerA.Font = new System.Drawing.Font("Verdana", 12F);
            this.txt_answerA.Location = new System.Drawing.Point(442, 438);
            this.txt_answerA.Name = "txt_answerA";
            this.txt_answerA.Size = new System.Drawing.Size(200, 38);
            this.txt_answerA.TabIndex = 24;
            this.txt_answerA.ThemeName = "TelerikMetro";
            this.txt_answerA.Validating += new System.ComponentModel.CancelEventHandler(this.txt_answerA_Validating);
            // 
            // radLabel4
            // 
            this.radLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel4.Font = new System.Drawing.Font("Verdana", 12F);
            this.radLabel4.Location = new System.Drawing.Point(38, 404);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(80, 23);
            this.radLabel4.TabIndex = 27;
            this.radLabel4.Text = "Content:";
            this.radLabel4.ThemeName = "TelerikMetro";
            // 
            // txt_content
            // 
            this.txt_content.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_content.Font = new System.Drawing.Font("Verdana", 12F);
            this.txt_content.Location = new System.Drawing.Point(38, 438);
            this.txt_content.Multiline = true;
            this.txt_content.Name = "txt_content";
            // 
            // 
            // 
            this.txt_content.RootElement.StretchVertically = true;
            this.txt_content.Size = new System.Drawing.Size(338, 115);
            this.txt_content.TabIndex = 26;
            this.txt_content.ThemeName = "TelerikMetro";
            this.txt_content.Validating += new System.ComponentModel.CancelEventHandler(this.txt_content_Validating);
            // 
            // btn_add
            // 
            this.btn_add.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_add.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold);
            this.btn_add.Location = new System.Drawing.Point(38, 576);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(167, 60);
            this.btn_add.TabIndex = 38;
            this.btn_add.Text = "ADD";
            this.btn_add.ThemeName = "TelerikMetro";
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_update
            // 
            this.btn_update.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_update.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold);
            this.btn_update.Location = new System.Drawing.Point(344, 576);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(167, 60);
            this.btn_update.TabIndex = 39;
            this.btn_update.Text = "UPDATE";
            this.btn_update.ThemeName = "TelerikMetro";
            this.btn_update.Click += new System.EventHandler(this.btn_update_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_delete.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold);
            this.btn_delete.Location = new System.Drawing.Point(657, 576);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(167, 60);
            this.btn_delete.TabIndex = 39;
            this.btn_delete.Text = "DELETE";
            this.btn_delete.ThemeName = "TelerikMetro";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_exit.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold);
            this.btn_exit.Location = new System.Drawing.Point(998, 576);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(167, 60);
            this.btn_exit.TabIndex = 41;
            this.btn_exit.Text = "SIGN OUT";
            this.btn_exit.ThemeName = "TelerikMetro";
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // ddbtn_level
            // 
            this.ddbtn_level.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.mnitem_easy,
            this.mnitem_medium,
            this.mnitem_hard,
            this.mnitem_all});
            this.ddbtn_level.Location = new System.Drawing.Point(37, 23);
            this.ddbtn_level.Name = "ddbtn_level";
            this.ddbtn_level.Size = new System.Drawing.Size(122, 36);
            this.ddbtn_level.TabIndex = 42;
            this.ddbtn_level.Text = "All";
            this.ddbtn_level.ThemeName = "TelerikMetro";
            // 
            // mnitem_easy
            // 
            this.mnitem_easy.Name = "mnitem_easy";
            this.mnitem_easy.Text = "Easy";
            this.mnitem_easy.Click += new System.EventHandler(this.mnitem_easy_Click);
            // 
            // mnitem_medium
            // 
            this.mnitem_medium.Name = "mnitem_medium";
            this.mnitem_medium.Text = "Medium";
            this.mnitem_medium.Click += new System.EventHandler(this.mnitem_medium_Click);
            // 
            // mnitem_hard
            // 
            this.mnitem_hard.Name = "mnitem_hard";
            this.mnitem_hard.Text = "Hard";
            this.mnitem_hard.Click += new System.EventHandler(this.mnitem_hard_Click);
            // 
            // mnitem_all
            // 
            this.mnitem_all.Name = "mnitem_all";
            this.mnitem_all.Text = "All";
            this.mnitem_all.Click += new System.EventHandler(this.mnitem_all_Click);
            // 
            // valid_question
            // 
            this.valid_question.ContainerControl = this;
            // 
            // dropdownbtn_level
            // 
            this.dropdownbtn_level.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dropdownbtn_level.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.menuitem_easy,
            this.menuitem_medium,
            this.menuitem_hard});
            this.dropdownbtn_level.Location = new System.Drawing.Point(964, 515);
            this.dropdownbtn_level.Name = "dropdownbtn_level";
            this.dropdownbtn_level.Size = new System.Drawing.Size(200, 38);
            this.dropdownbtn_level.TabIndex = 43;
            this.dropdownbtn_level.Text = "Dễ";
            this.dropdownbtn_level.ThemeName = "TelerikMetro";
            // 
            // menuitem_easy
            // 
            this.menuitem_easy.AccessibleDescription = "menuitem_easy";
            this.menuitem_easy.AccessibleName = "menuitem_easy";
            this.menuitem_easy.Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.menuitem_easy.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentBounds;
            this.menuitem_easy.Name = "menuitem_easy";
            this.menuitem_easy.Text = "Dễ";
            this.menuitem_easy.UseCompatibleTextRendering = false;
            this.menuitem_easy.Click += new System.EventHandler(this.menuitem_easy_Click);
            // 
            // menuitem_medium
            // 
            this.menuitem_medium.AccessibleDescription = "menuitem_medium";
            this.menuitem_medium.AccessibleName = "menuitem_medium";
            this.menuitem_medium.Name = "menuitem_medium";
            this.menuitem_medium.Text = "Trung Bình";
            this.menuitem_medium.Click += new System.EventHandler(this.menuitem_medium_Click);
            // 
            // menuitem_hard
            // 
            this.menuitem_hard.AccessibleDescription = "menuitem_hard";
            this.menuitem_hard.AccessibleName = "menuitem_hard";
            this.menuitem_hard.Name = "menuitem_hard";
            this.menuitem_hard.Text = "Khó";
            this.menuitem_hard.Click += new System.EventHandler(this.menuitem_hard_Click);
            // 
            // object_90f6713b_1cad_4883_ae3a_3486a1328c73
            // 
            this.object_90f6713b_1cad_4883_ae3a_3486a1328c73.Name = "object_90f6713b_1cad_4883_ae3a_3486a1328c73";
            this.object_90f6713b_1cad_4883_ae3a_3486a1328c73.StretchHorizontally = true;
            this.object_90f6713b_1cad_4883_ae3a_3486a1328c73.StretchVertically = true;
            // 
            // pic_exit
            // 
            this.pic_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pic_exit.BackColor = System.Drawing.Color.Transparent;
            this.pic_exit.Image = global::GraphicalUserInterface_GUI.Properties.Resources.delete;
            this.pic_exit.Location = new System.Drawing.Point(1161, 12);
            this.pic_exit.Name = "pic_exit";
            this.pic_exit.Size = new System.Drawing.Size(27, 25);
            this.pic_exit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pic_exit.TabIndex = 44;
            this.pic_exit.TabStop = false;
            this.pic_exit.Click += new System.EventHandler(this.pic_exit_Click);
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 740);
            this.Controls.Add(this.pic_exit);
            this.Controls.Add(this.dropdownbtn_level);
            this.Controls.Add(this.ddbtn_level);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_update);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.txt_result);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.txt_answerD);
            this.Controls.Add(this.txt_answerC);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.txt_answerB);
            this.Controls.Add(this.txt_answerA);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.txt_content);
            this.Controls.Add(this.gv_questions);
            this.Controls.Add(this.radLabel9);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Admin";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QuestionManagement";
            this.ThemeName = "TelerikMetro";
            this.Load += new System.EventHandler(this.Admin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_questions.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv_questions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_result)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_answerD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_answerC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_answerB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_answerA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_content)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_update)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddbtn_level)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.valid_question)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dropdownbtn_level)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_exit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadGridView gv_questions;
        private Telerik.WinControls.Themes.AquaTheme aquaTheme1;
        private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadTextBox txt_result;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox txt_answerD;
        private Telerik.WinControls.UI.RadTextBox txt_answerC;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox txt_answerB;
        private Telerik.WinControls.UI.RadTextBox txt_answerA;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txt_content;
        private Telerik.WinControls.UI.RadButton btn_add;
        private Telerik.WinControls.UI.RadButton btn_update;
        private Telerik.WinControls.UI.RadButton btn_delete;
        private Telerik.WinControls.UI.RadButton btn_exit;
        private Telerik.WinControls.UI.RadDropDownButton ddbtn_level;
        private Telerik.WinControls.UI.RadMenuItem mnitem_easy;
        private Telerik.WinControls.UI.RadMenuItem mnitem_medium;
        private Telerik.WinControls.UI.RadMenuItem mnitem_hard;
        private Telerik.WinControls.UI.RadMenuItem mnitem_all;
        private System.Windows.Forms.ErrorProvider valid_question;
        private Telerik.WinControls.UI.RadDropDownButton dropdownbtn_level;
        private Telerik.WinControls.UI.RadMenuItem menuitem_easy;
        private Telerik.WinControls.UI.RadMenuItem menuitem_medium;
        private Telerik.WinControls.UI.RadMenuItem menuitem_hard;
        private Telerik.WinControls.RootRadElement object_90f6713b_1cad_4883_ae3a_3486a1328c73;
        private System.Windows.Forms.PictureBox pic_exit;
    }
}
