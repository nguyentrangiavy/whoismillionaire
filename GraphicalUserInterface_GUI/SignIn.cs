﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer_BLL;
using DataAccessLayer_DAL;
using WMPLib;

namespace GraphicalUserInterface_GUI
{
    public partial class SignIn : Form
    {
        String userNamePlacehoder = "Tên đăng nhập";
        String passwordPlacehoder = "Mật khẩu";
        public static int id;
        public static string name;
        WindowsMediaPlayer player = new WindowsMediaPlayer();
        UserManagement userManagement = new UserManagement();
        public SignIn()
        {
            InitializeComponent();
            playMP3File("nhac-mo-dau.mp3", true, 50);
        }

        public void playMP3File(String fileName, bool loop, int volume)
        {
            string audio = Path.Combine(Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.Parent.FullName, @"audio\" + fileName);
            player.URL = audio;
            player.settings.setMode("loop", loop);
            player.settings.volume = volume;
            player.controls.play();
        }

        public void stopMP3File()
        {
            player.controls.stop();
        }
        
        public void clearFields()
        {
            txt_username.Text = userNamePlacehoder;
            txt_username.ForeColor = Color.Gray;
            txt_password.Text = passwordPlacehoder;
            txt_password.ForeColor = Color.Gray;
        }
        public void focusTextBox(System.Windows.Forms.TextBox textBox, String placehoder)
        {
            if (textBox.Text == placehoder)
            {
                textBox.Text = "";
                textBox.ForeColor = Color.Black;
            }
        }
        
        public void outFocusTextBox(System.Windows.Forms.TextBox textBox, String placehoder)
        {
            if (String.IsNullOrWhiteSpace(textBox.Text))
            {
                textBox.Text = placehoder;
                textBox.ForeColor = Color.Gray;
            }
        }
        
        private void btn_close_Click(object sender, EventArgs e)
        {
            DialogResult exitApp = MessageBox.Show("Bạn có muốn thoát khỏi trò chơi?", "Thoát", MessageBoxButtons.YesNo);
            if (exitApp == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            User user = userManagement.login(txt_username.Text,txt_password.Text);
            //check validation
            if (ValidateChildren()==true && txt_username.Text!=userNamePlacehoder && txt_password.Text!=passwordPlacehoder)
            {
                //check username and password
                if (user!=null)
                {
                    MessageBox.Show("Đăng nhập thành công!", "Đăng nhập");
                    clearFields();
                    id = user.id;
                    name = user.name;
                    this.Hide();
                    //check role
                    if (user.role.Trim() == "admin")
                    {
                        Admin admin = new Admin();
                        admin.Show();
                    }
                    else
                    {
                        Menu menu = new Menu();
                        menu.Show();
                        playMP3File("nhac-nen.mp3", true, 50);
                    }
                }
                else
                {
                    MessageBox.Show("Tài khoản hoặc mật khẩu không chính xác!", "Đăng nhập");
                }
            }
            else
            {
                MessageBox.Show("Vui lòng cung cấp đầy đủ thông tin đăng nhập!","Đăng nhập");
            }
        }

        private void label_signup_Click(object sender, EventArgs e)
        {
            SignUp signUp = new SignUp();
            this.Hide();
            signUp.Show();
        }

        private void label_signup_MouseHover(object sender, EventArgs e)
        {
            label_signup.ForeColor = Color.Gray;
        }

        private void label_signup_MouseLeave(object sender, EventArgs e)
        {
            label_signup.ForeColor = Color.DodgerBlue;
        }

        private void SignIn_Load(object sender, EventArgs e)
        {
            clearFields();
        }

        private void txt_username_Enter(object sender, EventArgs e)
        {
            focusTextBox(txt_username, userNamePlacehoder);
        }

        private void txt_username_Leave(object sender, EventArgs e)
        {
            outFocusTextBox(txt_username, userNamePlacehoder);
        }

        private void txt_password_Enter(object sender, EventArgs e)
        {
            focusTextBox(txt_password, passwordPlacehoder);
        }

        private void txt_password_Leave(object sender, EventArgs e)
        {
            outFocusTextBox(txt_password, passwordPlacehoder);
        }

        private void txt_username_Validated(object sender, EventArgs e)
        {
            if(txt_username.Text == userNamePlacehoder)
            {
                valid_signin.SetError(txt_username, "Tên đăng nhập không được bỏ trống!");
            }
            else
            {
                valid_signin.SetError(txt_username, "");
            }
        }

        private void txt_password_Validated(object sender, EventArgs e)
        {
            if (txt_password.Text == passwordPlacehoder)
            {
                valid_signin.SetError(txt_password, "Mật khẩu không được bỏ trống!");
            }
            else
            {
                valid_signin.SetError(txt_password, "");
            }
        }
    }
}
