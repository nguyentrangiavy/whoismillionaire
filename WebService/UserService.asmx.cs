﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DataAccessLayer_DAL;

namespace WebService
{
    /// <summary>
    /// Summary description for UserService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class UserService : System.Web.Services.WebService
    {
        UserDAO userDAO = new UserDAO();
        [WebMethod]
        public bool register(User user)
        {
            if (userDAO.createUser(user) == 1)
            {
                closeConnnection();
                return true;
            }
            closeConnnection();
            return false;
        }

        [WebMethod]
        public bool checkExist(String userName)
        {
            User user = userDAO.findUserByUserName(userName);
            closeConnnection();
            if (user != null)
            {
                return true;
            }
            return false;
        }

        [WebMethod]
        public User login(string userName, string password)
        {
            User user = userDAO.findUserByUserName(userName);
            closeConnnection();
            if (user != null)
            {
                if (user.password == password)
                {
                    return user;
                }
            }
            return null;
        }

        [WebMethod]
        public void closeConnnection()
        {
            userDAO.closeConnection();
        }
    }
}
