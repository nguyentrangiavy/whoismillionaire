﻿using DataAccessLayer_DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
namespace BussinessLogicLayer_BLL
{
    public class UserManagement
    {
        UserDAO userDAO = new UserDAO();
        public bool register(User user)
        {
            if (userDAO.createUser(user)==1)
            {
                closeConnnection();
                return true;
            }
            closeConnnection();
            return false;
        }

        public bool checkExist(String userName)
        {
            User user = userDAO.findUserByUserName(userName);
            closeConnnection();
            if (user!=null)
            {
                return true;
            }
            return false;
        }
        public User login(string userName, string password)
        {
            User user = userDAO.findUserByUserName(userName);
            closeConnnection();
            if (user!=null)
            {
                if (user.password == password)
                {
                    return user;
                }
            }
            return null;
        }
        public void closeConnnection()
        {
            userDAO.closeConnection();
        }
    }
}
