﻿using DataAccessLayer_DAL;
using System;
using System.Collections.Generic;
using System.Data;

namespace BussinessLogicLayer_BLL
{
    public class QuestionManagement
    {
        QuestionDAO questionDAO = new QuestionDAO();

        public List<Question> getQuestions(String level)
        {
            List<Question> list = new List<Question>();
            DataSet data = null;
            if (level == "All")
            {
                data = questionDAO.getAllQuestion();
            }
            else if(level == "Easy")
            {
                data = questionDAO.getEasyQuestions();
            }
            else if(level == "Medium")
            {
                data = questionDAO.getMediumQuestions();
            }
            else
            {
                data = questionDAO.getHardQuestions();
            }
            
            DataTable dataTable = data.Tables[0];
            foreach(DataRow row in dataTable.Rows)
            {
                Question question = new Question()
                {
                    id = row.Field<int>(0),
                    content = row.Field<String>(1),
                    answerA = row.Field<String>(2),
                    answerB = row.Field<String>(3),
                    answerC = row.Field<String>(4),
                    answerD = row.Field<String>(5),
                    result = row.Field<String>(6),
                    level = row.Field<String>(7)
                };
                list.Add(question);
            }
            return list;
        }
        public bool checkExistToAdd(String content)
        {
            String condition = "QuestionContent = N'" + content + "'";
            bool result = questionDAO.checkExist(condition);
            questionDAO.closeConnection();
            return result;
        }
        public bool checkExistToUpdate(String content, int id)
        {
            String condition = "QuestionContent = N'" + content + "' AND ID !=" + id;
            bool result = questionDAO.checkExist(condition);
            questionDAO.closeConnection();
            return result;
        }
        public bool addQuestion(Question question)
        {
            int result = questionDAO.insertQuestion(question);
            questionDAO.closeConnection();
            if (result == 1)
            {
                return true;
            }
            return false;
        }
        public bool updateQuestion(Question question)
        {
            int result = questionDAO.updateQuestion(question);
            questionDAO.closeConnection();
            if ( result == 1)
            {
                return true;
            }
            return false;
        }
        public bool deleteQuestion(int id)
        {
            int result = questionDAO.deleteQuestion(id);
            questionDAO.closeConnection();
            if (result == 1)
            {
                return true;
            }
            return false;
        }

        public List<Question> getRandomQuestions()
        {
            List<Question> list = new List<Question>();
            List<Question> easyList = getQuestions("Easy");
            List<Question> mediumList = getQuestions("Medium");
            List<Question> hardList = getQuestions("Hard");
            var random = new Random();
            while(list.Count<5)
            {
                int randomIndex = random.Next(easyList.Count);
                if(list.Find(ques => ques.id == easyList[randomIndex].id) == null)
                {
                    list.Add(easyList[randomIndex]);
                }
            }
            while (list.Count < 10)
            {
                int randomIndex = random.Next(mediumList.Count);
                if (list.Find(ques => ques.id == mediumList[randomIndex].id) == null)
                {
                    list.Add(mediumList[randomIndex]);
                }
            }
            while (list.Count < 15)
            {
                int randomIndex = random.Next(hardList.Count);
                if (list.Find(ques => ques.id == hardList[randomIndex].id) == null)
                {
                    list.Add(hardList[randomIndex]);
                }
            }
            return list;
        }
    }
}
