﻿using DataAccessLayer_DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLogicLayer_BLL
{
    public class RankManagement
    {
        RankDAO rankDAO = new RankDAO();
        public int addRank(Rank rank)
        {
            int result = rankDAO.createRank(rank);
            rankDAO.closeConnection();
            return result;
        }

        public int updateRank(Rank rank)
        {
            int result = rankDAO.updateRank(rank);
            rankDAO.closeConnection();
            return result;
        }

        public List<UserRank> getAll()
        {
            DataSet list = rankDAO.getRanks();
            List<UserRank> rankList = new List<UserRank>();
            foreach(DataRow row in list.Tables[0].Rows)
            {
                UserRank rank = new UserRank()
                {
                    Name = row.Field<string>("Name"),
                    Reward = row.Field<decimal>("Reward"),
                    Time = row.Field<DateTime>("Time"),
                    Rank = row.Field<Int64>("Ranking")
                };
                rankList.Add(rank);
            }
            return rankList;
        }

        public bool checkExist(int id)
        {
            bool result = rankDAO.checkExist(id);
            rankDAO.closeConnection();
            return result;
        }

        public bool checkReward(decimal reward, int id)
        {
            bool result = rankDAO.checkReward(reward, id);
            rankDAO.closeConnection();
            return result;
        }
    }
}
